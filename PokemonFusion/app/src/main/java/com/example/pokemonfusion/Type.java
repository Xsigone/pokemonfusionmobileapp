package com.example.pokemonfusion;

import java.io.Serializable;
import java.util.Hashtable;

public class Type implements Serializable {

    private int id;
    private String nom;
    private Hashtable<String, Float> table;

    public Type(){

    }

    public Type(int id, String nom, float acier, float combat, float dragon, float eau, float electrik, float fee, float feu, float glace, float insecte, float normal, float plante, float poison, float psy, float roche, float sol, float spectre, float tenebre, float vol) {
        this.id = id;
        this.nom = nom;
        this.table = new Hashtable<>();
        this.table.put("acier",acier) ;
        this.table.put("combat",combat) ;
        this.table.put("dragon",dragon) ;
        this.table.put("eau",eau) ;
        this.table.put("electrik",electrik) ;
        this.table.put("fee",fee) ;
        this.table.put("feu",feu) ;
        this.table.put("glace",glace) ;
        this.table.put("insecte",insecte) ;
        this.table.put("normal",normal) ;
        this.table.put("plante",plante) ;
        this.table.put("poison",poison) ;
        this.table.put("psy",psy) ;
        this.table.put("roche",roche) ;
        this.table.put("sol",sol) ;
        this.table.put("spectre",spectre) ;
        this.table.put("tenebre",tenebre) ;
        this.table.put("vol",vol) ;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public float getType(String type){
        return this.table.get(type);
    }
}
