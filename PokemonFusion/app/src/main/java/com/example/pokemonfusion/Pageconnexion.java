package com.example.pokemonfusion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Hashtable;

public class Pageconnexion extends AppCompatActivity {
    //private MediaPlayer mediaPlayer;
    private DBOpenHelper dbOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connexion);
        // Ouverture de la base de données
        dbOpenHelper = new DBOpenHelper(this);
        try {
            dbOpenHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            dbOpenHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

    }

    public void connexion(View view){
        Hashtable<String, Utilisateur> utilisateurs = dbOpenHelper.getUtilisateurHash();
        EditText ndc = findViewById(R.id.champNDC);
        EditText mdp = findViewById(R.id.champMDP);
        String nomCompte = ndc.getText().toString();
        String motDePasse = mdp.getText().toString();
        String mdpAttente = utilisateurs.get(nomCompte).getMdp();
        if(mdpAttente!=null && motDePasse.equals(mdpAttente)){
            Intent intent = new Intent(this, Acc.class);
            //intent.putExtra("val","kadamar");
            int idUt = utilisateurs.get(nomCompte).getId();
            intent.putExtra("idUt", idUt);
            intent.putExtra("teamselecte","aucune");
            startActivity(intent);
            Toast toastreussite = Toast.makeText(getApplicationContext(), "Connexion Reussie",Toast.LENGTH_SHORT);
            toastreussite.show();
        }
        else{
            Toast toastrate = Toast.makeText(getApplicationContext(), "Nom d'utilisateur ou mot de passe incorect",Toast.LENGTH_SHORT);
            toastrate.show();
        }
    }

    public void creerCompte(View view){
        Intent intent = new Intent(this, CreerCompte.class);
        startActivity(intent);
    }

}
