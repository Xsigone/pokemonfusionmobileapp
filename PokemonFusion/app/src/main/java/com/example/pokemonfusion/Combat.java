package com.example.pokemonfusion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.SQLException;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Combat extends AppCompatActivity {
    //private MediaPlayer mediaPlayer;
    private DBOpenHelper dbOpenHelper;
    private GestionCombat combat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.combat);
        dbOpenHelper = new DBOpenHelper(this);
        try {
            dbOpenHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            dbOpenHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        Intent intent = getIntent();
        this.combat = (GestionCombat)getIntent().getSerializableExtra("combat");

        // Affichage du pokemon du joueur

        reload();
        //Lancement de la musique au lancement du jeux
        //this.mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.monsons);
        //mediaPlayer.start();

    }

    public boolean reload(){
        ImageView imagePoke = (ImageView) findViewById(R.id.monpokemon);
        TextView nomPoke =(TextView) findViewById(R.id.textView2);
        TextView pvPoke =(TextView) findViewById(R.id.textView7);
        ProgressBar pvPB = (ProgressBar)findViewById(R.id.progressBar2);

        Pokemon pokemon = this.combat.getPokemonEquipe1();
        imagePoke.setImageResource(getResources().getIdentifier(pokemon.getNom().toLowerCase()+"face", "drawable", getPackageName()));
        nomPoke.setText(pokemon.getNom());
        String pv = pokemon.getPvActuel() + "/" + pokemon.getStat().getPv() + " " + pokemon.getStatut().getNom();
        pvPoke.setText(pv);
        pvPB.setProgress((int)(((float)pokemon.getPvActuel() / (float)pokemon.getStat().getPv()) * 100), true);

        Button atk1=findViewById(R.id.atk1);
        Button atk2=findViewById(R.id.atk2);
        Button atk3=findViewById(R.id.atk3);
        Button atk4=findViewById(R.id.atk4);

        atk1.setText(pokemon.getAttaque1().getNom());
        atk2.setText(pokemon.getAttaque2().getNom());
        atk3.setText(pokemon.getAttaque3().getNom());
        atk4.setText(pokemon.getAttaque4().getNom());

        atk1.setContentDescription(""+pokemon.getAttaque1().getId());
        atk2.setContentDescription(""+pokemon.getAttaque2().getId());
        atk3.setContentDescription(""+pokemon.getAttaque3().getId());
        atk4.setContentDescription(""+pokemon.getAttaque4().getId());

        changerBackButton(atk1, pokemon.getAttaque1());
        changerBackButton(atk2, pokemon.getAttaque2());
        changerBackButton(atk3, pokemon.getAttaque3());
        changerBackButton(atk4, pokemon.getAttaque4());

        // Affichage du pokemon de l'adversaire

        ImageView imagePokeA = (ImageView) findViewById(R.id.imageView12);
        TextView nomPokeA =(TextView) findViewById(R.id.textView3);
        TextView pvPokeA =(TextView) findViewById(R.id.textView6);
        ProgressBar pvPBA = (ProgressBar)findViewById(R.id.progressBar);

        Pokemon pokemonA = this.combat.getPokemonEquipe2();
        imagePokeA.setImageResource(getResources().getIdentifier(pokemonA.getNom().toLowerCase(), "drawable", getPackageName()));
        nomPokeA.setText(pokemonA.getNom());
        String pvA = pokemonA.getPvActuel() + "/" + pokemonA.getStat().getPv() + " " + pokemonA.getStatut().getNom();
        pvPokeA.setText(pvA);
        pvPBA.setProgress((int)(((float)pokemonA.getPvActuel() / (float)pokemonA.getStat().getPv()) * 100), true);

        return verifKo();

    }

    //fonction permettant d'aller sur la view switchpkm depuis main
    public void switchpkm(View view){
        this.combat.gererStatut();
        Intent intent = new Intent(this, SwitchPokemon.class);
        intent.putExtra("switch", "switch");
        intent.putExtra("combat", this.combat);
        startActivity(intent);

    }

    public void abandon(View view){
        Intent intent = new Intent(this, Acc.class);
        intent.putExtra("teamselecte","aucune");
        startActivity(intent);

    }

    public boolean verifKo(){
        if (this.combat.getPokemonEquipe1().getPvActuel()==0){
            this.combat.getPokemonEquipe1().setMort(true);
            boolean teamKo = true;
            for (Pokemon pok : this.combat.getEquipe1().getListePokemons()){
                if (!pok.isMort()){
                    teamKo=false;
                }
            }
            if(!teamKo) {
                Intent intent = new Intent(this, SwitchPokemon.class);
                intent.putExtra("switch", "ko");
                intent.putExtra("combat", this.combat);
                startActivity(intent);
            }else{
                Toast toast = Toast.makeText(getApplicationContext(), "Vous avez perdu !",Toast.LENGTH_SHORT);
                toast.show();
                Intent intent = new Intent(this, Acc.class);
                intent.putExtra("teamselecte","aucune");
                startActivity(intent);
            }
            return true;
        }

        if (this.combat.getPokemonEquipe2().getPvActuel()==0){
            this.combat.getPokemonEquipe2().setMort(true);
            Toast toast = Toast.makeText(getApplicationContext(), "Le pokemon adverse est mort", Toast.LENGTH_SHORT);
            toast.show();
            boolean teamKo = true;
            for (Pokemon pok : this.combat.getEquipe2().getListePokemons()){
                if (!pok.isMort()){
                    teamKo=false;
                }
            }
            if(!teamKo) {
                this.combat.switchAdverse();
                reload();
            }else{
                Toast toast2 = Toast.makeText(getApplicationContext(), "Vous avez gagné !",Toast.LENGTH_SHORT);
                toast2.show();
                Intent intent = new Intent(this, Acc.class);
                intent.putExtra("teamselecte","aucune");
                startActivity(intent);
            }
            return true;
        }
        return false;
    }

    public void attaquer(View view){
        this.combat.gererStatut();
        String idAttaque = (String)view.getContentDescription();
        int idAttaqueI = Integer.parseInt(idAttaque.trim());
        Attaque attaque = dbOpenHelper.getAttaqueById(idAttaqueI);
        if (this.combat.getPokemonEquipe1().getStat().getVitesse() > this.combat.getPokemonEquipe2().getStat().getVitesse()){
            String efficace = this.combat.attaqueJoueur(attaque);
            if (!efficace.equals("")){
                efficace = " et " + efficace;
            }
            String texte = "Le "+this.combat.getPokemonEquipe1().getNom() + " allié utilise "+attaque.getNom() + efficace;
            Toast toast = Toast.makeText(getApplicationContext(), texte,Toast.LENGTH_SHORT);
            toast.show();
            if (!reload()) {
                List<Attaque> listeAttaque = new ArrayList<>();
                listeAttaque.add(this.combat.getPokemonEquipe2().getAttaque1());
                listeAttaque.add(this.combat.getPokemonEquipe2().getAttaque2());
                listeAttaque.add(this.combat.getPokemonEquipe2().getAttaque3());
                listeAttaque.add(this.combat.getPokemonEquipe2().getAttaque4());

                String puissance = "";
                Random random = new Random();
                int nb;
                nb = 1+random.nextInt(4-1);

                String efficace2 = this.combat.attaqueAdverse(listeAttaque.get(nb));
                if (!efficace2.equals("")){
                    efficace2 = " et " + efficace2;
                }
                String texte2 = "Le "+this.combat.getPokemonEquipe2().getNom() + " adverse utilise "+this.combat.getPokemonEquipe2().getAttaque1().getNom() + efficace2;
                Toast toast2 = Toast.makeText(getApplicationContext(), texte2,Toast.LENGTH_SHORT);
                toast2.show();

                reload();
            }

        }
        else{
            List<Attaque> listeAttaque = new ArrayList<>();
            listeAttaque.add(this.combat.getPokemonEquipe2().getAttaque1());
            listeAttaque.add(this.combat.getPokemonEquipe2().getAttaque2());
            listeAttaque.add(this.combat.getPokemonEquipe2().getAttaque3());
            listeAttaque.add(this.combat.getPokemonEquipe2().getAttaque4());

            String puissance = "";
            Random random = new Random();
            int nb;
            nb = 1+random.nextInt(4-1);

            String efficace2 = this.combat.attaqueAdverse(listeAttaque.get(nb));
            if (!efficace2.equals("")){
                efficace2 = " et " + efficace2;
            }
            String texte2 = "Le "+this.combat.getPokemonEquipe2().getNom() + " adverse utilise "+this.combat.getPokemonEquipe2().getAttaque1().getNom() + efficace2;
            Toast toast2 = Toast.makeText(getApplicationContext(), texte2,Toast.LENGTH_SHORT);
            toast2.show();
            if(!reload()) {
                String efficace = this.combat.attaqueJoueur(attaque);
                if (!efficace.equals("")){
                    efficace = " et " + efficace;
                }
                String texte = "Le "+this.combat.getPokemonEquipe1().getNom() + " allié utilise "+attaque.getNom() + efficace;
                Toast toast = Toast.makeText(getApplicationContext(), texte,Toast.LENGTH_SHORT);
                toast.show();
                reload();
            }
        }
    }

    public void changerBackButton(Button b, Attaque atk){
        if(atk.getType1().getId() == atk.getType2().getId()){
            if(atk.getType1().getNom().equals("eau")){
                b.setBackgroundColor(Color.rgb(4,100,223));
            }
            if(atk.getType1().getNom().equals("feu")){
                b.setBackgroundColor(Color.rgb(250,82,41));
            }
            if(atk.getType1().getNom().equals("glace")){
                b.setBackgroundColor(Color.rgb(147,189,244));
            }
            if(atk.getType1().getNom().equals("plante")){
                b.setBackgroundColor(Color.rgb(75,219,21));
            }
            if(atk.getType1().getNom().equals("sol")){
                b.setBackgroundColor(Color.rgb(219,159,21));
            }
            if(atk.getType1().getNom().equals("roche")){
                b.setBackgroundColor(Color.rgb(208,177,60));
            }
            if(atk.getType1().getNom().equals("psy")){
                b.setBackgroundColor(Color.rgb(220,20,236));
            }
            if(atk.getType1().getNom().equals("poison")){
                b.setBackgroundColor(Color.rgb(122,37,129));
            }

        }
        if((atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("insecte")) || (atk.getType1().getNom().equals("insecte") && atk.getType2().getNom().equals("feu")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientinsectefeu));
        }
        if((atk.getType1().getNom().equals("plante") && atk.getType2().getNom().equals("normal")) || (atk.getType1().getNom().equals("normal") && atk.getType2().getNom().equals("plante")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientplantenormal));
        }
        if((atk.getType1().getNom().equals("plante") && atk.getType2().getNom().equals("electrik")) || (atk.getType1().getNom().equals("electrik") && atk.getType2().getNom().equals("plante")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientplanteelectrique));
        }
        if((atk.getType1().getNom().equals("fee") && atk.getType2().getNom().equals("roche")) || (atk.getType1().getNom().equals("roche") && atk.getType2().getNom().equals("fee")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientfeeroche));
        }
        if((atk.getType1().getNom().equals("eau") && atk.getType2().getNom().equals("feu")) || (atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("eau")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradienteaufeu));
        }
        if((atk.getType1().getNom().equals("vol") && atk.getType2().getNom().equals("combat")) || (atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("vol")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientvolcombat));
        }
        if((atk.getType1().getNom().equals("psy") && atk.getType2().getNom().equals("tenebre")) || (atk.getType1().getNom().equals("tenebre") && atk.getType2().getNom().equals("psy")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientpsytenebre));
        }
        if((atk.getType1().getNom().equals("acier") && atk.getType2().getNom().equals("combat")) || (atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("acier")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientaciercombat));
        }
        if((atk.getType1().getNom().equals("eau") && atk.getType2().getNom().equals("normal")) || (atk.getType1().getNom().equals("normal") && atk.getType2().getNom().equals("eau")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradienteaunormal));
        }
        if((atk.getType1().getNom().equals("psy") && atk.getType2().getNom().equals("normal")) || (atk.getType1().getNom().equals("normal") && atk.getType2().getNom().equals("psy")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientpsynormal));
        }
        if((atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("glace")) || (atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("glace")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientfeuglace));
        }
        if((atk.getType1().getNom().equals("fee") && atk.getType2().getNom().equals("plante")) || (atk.getType1().getNom().equals("plante") && atk.getType2().getNom().equals("fee")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientfeeplante));
        }
        if((atk.getType1().getNom().equals("eau") && atk.getType2().getNom().equals("acier")) || (atk.getType1().getNom().equals("acier") && atk.getType2().getNom().equals("eau")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradienteauacier));
        }
        if((atk.getType1().getNom().equals("glace") && atk.getType2().getNom().equals("combat")) || (atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("glace")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientglacecombat));
        }
        if((atk.getType1().getNom().equals("dragon") && atk.getType2().getNom().equals("fee")) || (atk.getType1().getNom().equals("fee") && atk.getType2().getNom().equals("dragon")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientfeedragon));
        }
        if((atk.getType1().getNom().equals("spectre") && atk.getType2().getNom().equals("psy")) || (atk.getType1().getNom().equals("psy") && atk.getType2().getNom().equals("spectre")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientpsyspectre));
        }
        if((atk.getType1().getNom().equals("insecte") && atk.getType2().getNom().equals("spectre")) || (atk.getType1().getNom().equals("spectre") && atk.getType2().getNom().equals("insecte")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientinsectespectre));
        }
        if((atk.getType1().getNom().equals("dragon") && atk.getType2().getNom().equals("electrik")) || (atk.getType1().getNom().equals("electrik") && atk.getType2().getNom().equals("dragon")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientelectriquedragon));
        }
        if((atk.getType1().getNom().equals("tenebre") && atk.getType2().getNom().equals("feu")) || (atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("tenebre")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradienttenebrefeu));
        }
        if((atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("eau")) || (atk.getType1().getNom().equals("eau") && atk.getType2().getNom().equals("combat")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradienteaucombat));
        }
        if((atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("feu")) || (atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("combat")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientfeucombat));
        }
        if((atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("feu")) || (atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("combat")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientfeucombat));
        }
        if((atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("poison")) || (atk.getType1().getNom().equals("poison") && atk.getType2().getNom().equals("combat")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientpoisoncombat));
        }
        if((atk.getType1().getNom().equals("vol") && atk.getType2().getNom().equals("poison")) || (atk.getType1().getNom().equals("poison") && atk.getType2().getNom().equals("vol")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientpoisonvol));
        }
        if((atk.getType1().getNom().equals("sol") && atk.getType2().getNom().equals("roche")) || (atk.getType1().getNom().equals("roche") && atk.getType2().getNom().equals("sol")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientrochesol));
        }
        if((atk.getType1().getNom().equals("sol") && atk.getType2().getNom().equals("insecte")) || (atk.getType1().getNom().equals("insecte") && atk.getType2().getNom().equals("sol")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientinsectesol));
        }
        if((atk.getType1().getNom().equals("vol") && atk.getType2().getNom().equals("plante")) || (atk.getType1().getNom().equals("plante") && atk.getType2().getNom().equals("vol")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientplantevol));
        }
        if((atk.getType1().getNom().equals("roche") && atk.getType2().getNom().equals("psy")) || (atk.getType1().getNom().equals("psy") && atk.getType2().getNom().equals("roche")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientrochepsy));
        }
        if((atk.getType1().getNom().equals("roche") && atk.getType2().getNom().equals("feu")) || (atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("roche")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientrochefeu));
        }
        if((atk.getType1().getNom().equals("normal") && atk.getType2().getNom().equals("combat")) || (atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("normal")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientnormalcombat));
        }

    }

}
