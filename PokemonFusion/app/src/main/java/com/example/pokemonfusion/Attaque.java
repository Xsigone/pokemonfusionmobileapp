package com.example.pokemonfusion;

import java.io.Serializable;

class Attaque implements Serializable {

    private int id;
    private String nom;
    private Type type1, type2;
    private int puissance, precision, pp, relance, typeAtk, vieDonnee, chanceDonneStatut, priorite, contreCoup;
    private float changeAtkPhys, changeAtkSpe, changeSpeed, changeDefPhys, changeDefSpe;
    private Statut statutEnnemie, statutLanceur;

    public Attaque(){}

    public Attaque(int id, String nom, Type type1, Type type2, int puissance, int precision, int pp, int relance, float changeAtkPhys, float changeAtkSpe, float changeSpeed, float changeDefPhys, float changeDefSpe, int typeAtk, int vieDonnee, int chanceDonneStatut, int priorite, int contreCoup, Statut statutEnnemie, Statut statutLanceur) {
        this.id = id;
        this.nom = nom;
        this.type1 = type1;
        this.type2 = type2;
        this.puissance = puissance;
        this.precision = precision;
        this.pp = pp;
        this.relance = relance;
        this.changeAtkPhys = changeAtkPhys;
        this.changeAtkSpe = changeAtkSpe;
        this.changeSpeed = changeSpeed;
        this.changeDefPhys = changeDefPhys;
        this.changeDefSpe = changeDefSpe;
        this.typeAtk = typeAtk;
        this.vieDonnee = vieDonnee;
        this.chanceDonneStatut = chanceDonneStatut;
        this.priorite = priorite;
        this.contreCoup = contreCoup;
        this.statutEnnemie = statutEnnemie;
        this.statutLanceur = statutLanceur;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public Type getType1() {
        return type1;
    }

    public Type getType2() {
        return type2;
    }

    public int getPuissance() {
        return puissance;
    }

    public int getPrecision() {
        return precision;
    }

    public int getPp() {
        return pp;
    }

    public int getRelance() {
        return relance;
    }

    public float getChangeAtkPhys() {
        return changeAtkPhys;
    }

    public float getChangeAtkSpe() {
        return changeAtkSpe;
    }

    public float getChangeSpeed() {
        return changeSpeed;
    }

    public float getChangeDefPhys() {
        return changeDefPhys;
    }

    public float getChangeDefSpe() {
        return changeDefSpe;
    }

    public int getTypeAtk() {
        return typeAtk;
    }

    public int getVieDonnee() {
        return vieDonnee;
    }

    public int getChanceDonneStatut() {
        return chanceDonneStatut;
    }

    public int getPriorite() {
        return priorite;
    }

    public int getContreCoup() {
        return contreCoup;
    }

    public void diminuePP(){this.pp-=1;}

    public Statut getStatutEnnemie() {
        return statutEnnemie;
    }

    public Statut getStatutLanceur() {
        return statutLanceur;
    }
}
