package com.example.pokemonfusion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class ModifPokemon extends AppCompatActivity {
    //private MediaPlayer mediaPlayer;
    private DBOpenHelper dbOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modifpokemon);
        // Ouverture de la base de données
        dbOpenHelper = new DBOpenHelper(this);
        try {
            dbOpenHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            dbOpenHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }


        Intent intent = getIntent();
        int idpoke = intent.getIntExtra("idPoke",0);
        Pokemon pkm = dbOpenHelper.getPokemonById(idpoke);

        ImageView image = (ImageView) findViewById(R.id.imgpkm);
        TextView textView =(TextView) findViewById(R.id.nomPoke);
        image.setImageResource(getResources().getIdentifier(pkm.getNom().toLowerCase(), "drawable", getPackageName()));
        textView.setText(pkm.getNom());

        Button atk1 = findViewById(R.id.atk1);
        Button atk2= findViewById(R.id.atk2);
        Button atk3 = findViewById(R.id.atk3);
        Button atk4 = findViewById(R.id.atk4);
        atk1.setText(pkm.getAttaque1().getNom()+"");
        atk2.setText(pkm.getAttaque2().getNom()+"");
        atk3.setText(pkm.getAttaque3().getNom()+"");
        atk4.setText(pkm.getAttaque4().getNom()+"");

        Button objet = findViewById(R.id.objet);
        objet.setText(pkm.getObjet().getNom()+"");
        Button talent = findViewById(R.id.talent);
        talent.setText(pkm.getTalent().getNom()+"");

        changerBackButton(atk1,pkm.getAttaque1());
        changerBackButton(atk2,pkm.getAttaque2());
        changerBackButton(atk3,pkm.getAttaque3());
        changerBackButton(atk4,pkm.getAttaque4());

    }

    public void retour(View view){
        Intent intentrecup = getIntent();
        String teamSelect = intentrecup.getStringExtra("teamselecte");

        Intent intentrec = getIntent();
        String val = intentrec.getStringExtra("idTeam");

        Intent intent = new Intent(this, ModifTeam.class);
        intent.putExtra("idTeam", val);
        intent.putExtra("teamselecte",teamSelect);
        startActivity(intent);
    }



    public void detailsAtk1(View view){

        Intent intent = getIntent();
        int idpoke = intent.getIntExtra("idPoke",0);
        Pokemon pkm = dbOpenHelper.getPokemonById(idpoke);
        String texte = "PP : "+pkm.getAttaque1().getPp()+" | Puissance : "+pkm.getAttaque1().getPuissance();
        if(pkm.getAttaque1().getStatutEnnemie().getId()!=9){
            texte+=" | peux appliquer l'état "+pkm.getAttaque1().getStatutEnnemie().getNom()+" avec une chance de "+pkm.getAttaque1().getChanceDonneStatut()+" %";
        }
        if(pkm.getAttaque1().getChangeAtkPhys()>1.0){
            texte+=" | augmente l'attaque physique";
        }
        if(pkm.getAttaque1().getChangeAtkSpe()>1.0){
            texte+=" | augmente l'attaque spécial";
        }
        if(pkm.getAttaque1().getChangeDefPhys()>1.0){
            texte+=" | augmente la defense physique";
        }
        if(pkm.getAttaque1().getChangeDefSpe()>1.0){
            texte+=" | augmente la defense spécial";
        }
        if(pkm.getAttaque1().getChangeSpeed()>1.0){
            texte+=" | augmente la vitesse";
        }
        if(pkm.getAttaque1().getContreCoup()!=0) {
            texte+=" | Cette attaque blesse aussi le lanceur";
        }
        if(pkm.getAttaque1().getPriorite()>0){
            texte+=" | Cette attaque peux se faire en premiere";
        }
        if(pkm.getAttaque1().getVieDonnee()!=0){
            texte+= " | cette attaque rend de la vie au lanceur";
        }
        if(pkm.getAttaque1().getStatutLanceur().getId()!=9){
            texte+=" | cette attaque applique l'etat "+pkm.getAttaque1().getStatutLanceur().getNom()+" au lanceur";
        }
        Toast descriAtk = Toast.makeText(getApplicationContext(), texte,Toast.LENGTH_LONG);
        descriAtk.show();
    }
    public void detailsAtk2(View view){

        Intent intent = getIntent();
        int idpoke = intent.getIntExtra("idPoke",0);
        Pokemon pkm = dbOpenHelper.getPokemonById(idpoke);
        String texte = "PP : "+pkm.getAttaque2().getPp()+" | Puissance : "+pkm.getAttaque2().getPuissance();
        if(pkm.getAttaque2().getStatutEnnemie().getId()!=9){
            texte+=" | peux appliquer l'état "+pkm.getAttaque2().getStatutEnnemie().getNom()+" avec une chance de "+pkm.getAttaque2().getChanceDonneStatut()+" %";
        }
        if(pkm.getAttaque2().getChangeAtkPhys()>1){
            texte+=" | augmente l'attaque physique";
        }
        if(pkm.getAttaque2().getChangeAtkSpe()>1){
            texte+=" | augmente l'attaque spécial";
        }
        if(pkm.getAttaque2().getChangeDefPhys()>1){
            texte+=" | augmente la defense physique";
        }
        if(pkm.getAttaque2().getChangeDefSpe()>1){
            texte+=" | augmente la defense spécial";
        }
        if(pkm.getAttaque2().getChangeSpeed()>1){
            texte+=" | augmente la vitesse";
        }
        if(pkm.getAttaque2().getContreCoup()!=0) {
            texte+=" | Cette attaque blesse aussi le lanceur";
        }
        if(pkm.getAttaque2().getPriorite()>0){
            texte+=" | Cette attaque peux se faire en premiere";
        }
        if(pkm.getAttaque2().getVieDonnee()!=0){
            texte+= " | cette attaque rend de la vie au lanceur";
        }
        if(pkm.getAttaque2().getStatutLanceur().getId()!=9){
            texte+=" | cette attaque applique l'etat "+pkm.getAttaque2().getStatutLanceur().getNom()+" au lanceur";
        }
        Toast descriAtk = Toast.makeText(getApplicationContext(), texte,Toast.LENGTH_LONG);
        descriAtk.show();
    }
    public void detailsAtk3(View view){

        Intent intent = getIntent();
        int idpoke = intent.getIntExtra("idPoke",0);
        Pokemon pkm = dbOpenHelper.getPokemonById(idpoke);
        String texte = "PP : "+pkm.getAttaque3().getPp()+" | Puissance : "+pkm.getAttaque3().getPuissance();
        if(pkm.getAttaque3().getStatutEnnemie().getId()!=9){
            texte+=" | peux appliquer l'état "+pkm.getAttaque3().getStatutEnnemie().getNom()+" avec une chance de "+pkm.getAttaque3().getChanceDonneStatut()+" %";
        }
        if(pkm.getAttaque3().getChangeAtkPhys()>1){
            texte+=" | augmente l'attaque physique";
        }
        if(pkm.getAttaque3().getChangeAtkSpe()>1){
            texte+=" | augmente l'attaque spécial";
        }
        if(pkm.getAttaque3().getChangeDefPhys()>1){
            texte+=" | augmente la defense physique";
        }
        if(pkm.getAttaque3().getChangeDefSpe()>1){
            texte+=" | augmente la defense spécial";
        }
        if(pkm.getAttaque3().getChangeSpeed()>1){
            texte+=" | augmente la vitesse";
        }
        if(pkm.getAttaque3().getContreCoup()!=0) {
            texte+=" | Cette attaque blesse aussi le lanceur";
        }
        if(pkm.getAttaque3().getPriorite()>0){
            texte+=" | Cette attaque peux se faire en premiere";
        }
        if(pkm.getAttaque3().getVieDonnee()!=0){
            texte+= " | cette attaque rend de la vie au lanceur";
        }
        if(pkm.getAttaque3().getStatutLanceur().getId()!=9){
            texte+=" | cette attaque applique l'etat "+pkm.getAttaque3().getStatutLanceur().getNom()+" au lanceur";
        }

        Toast descriAtk = Toast.makeText(getApplicationContext(), texte,Toast.LENGTH_LONG);
        descriAtk.show();
    }
    public void detailsAtk4(View view){

        Intent intent = getIntent();
        int idpoke = intent.getIntExtra("idPoke",0);
        Pokemon pkm = dbOpenHelper.getPokemonById(idpoke);
        String texte = "PP : "+pkm.getAttaque4().getPp()+" | Puissance : "+pkm.getAttaque4().getPuissance();
        if(pkm.getAttaque4().getStatutEnnemie().getId()!=9){
            texte+=" | peux appliquer l'état "+pkm.getAttaque4().getStatutEnnemie().getNom()+" avec une chance de "+pkm.getAttaque4().getChanceDonneStatut()+ " %";
        }
        if(pkm.getAttaque4().getChangeAtkPhys()>1){
            texte+=" | augmente l'attaque physique";
        }
        if(pkm.getAttaque4().getChangeAtkSpe()>1){
            texte+=" | augmente l'attaque spécial";
        }
        if(pkm.getAttaque4().getChangeDefPhys()>1){
            texte+=" | augmente la defense physique";
        }
        if(pkm.getAttaque4().getChangeDefSpe()>1){
            texte+=" | augmente la defense spécial";
        }
        if(pkm.getAttaque4().getChangeSpeed()>1){
            texte+=" | augmente la vitesse";
        }
        if(pkm.getAttaque4().getContreCoup()!=0) {
            texte+=" | Cette attaque blesse aussi le lanceur";
        }
        if(pkm.getAttaque4().getPriorite()>0){
            texte+=" | Cette attaque peux se faire en premiere";
        }
        if(pkm.getAttaque4().getVieDonnee()!=0){
            texte+= " | cette attaque rend de la vie au lanceur";
        }
        if(pkm.getAttaque4().getStatutLanceur().getId()!=9){
            texte+=" | cette attaque applique l'etat "+pkm.getAttaque4().getStatutLanceur().getNom()+" au lanceur";
        }
        Toast descriAtk = Toast.makeText(getApplicationContext(), texte,Toast.LENGTH_LONG);
        descriAtk.show();
    }

    public void changerBackButton(Button b, Attaque atk){
        if(atk.getType1().getId() == atk.getType2().getId()){
            if(atk.getType1().getNom().equals("eau")){
                b.setBackgroundColor(Color.rgb(4,100,223));
            }
            if(atk.getType1().getNom().equals("feu")){
                b.setBackgroundColor(Color.rgb(250,82,41));
            }
            if(atk.getType1().getNom().equals("glace")){
                b.setBackgroundColor(Color.rgb(147,189,244));
            }
            if(atk.getType1().getNom().equals("plante")){
                b.setBackgroundColor(Color.rgb(75,219,21));
            }
            if(atk.getType1().getNom().equals("sol")){
                b.setBackgroundColor(Color.rgb(219,159,21));
            }
            if(atk.getType1().getNom().equals("roche")){
                b.setBackgroundColor(Color.rgb(208,177,60));
            }
            if(atk.getType1().getNom().equals("psy")){
                b.setBackgroundColor(Color.rgb(220,20,236));
            }
            if(atk.getType1().getNom().equals("poison")){
                b.setBackgroundColor(Color.rgb(122,37,129));
            }

        }
        if((atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("insecte")) || (atk.getType1().getNom().equals("insecte") && atk.getType2().getNom().equals("feu")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientinsectefeu));
        }
        if((atk.getType1().getNom().equals("plante") && atk.getType2().getNom().equals("normal")) || (atk.getType1().getNom().equals("normal") && atk.getType2().getNom().equals("plante")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientplantenormal));
        }
        if((atk.getType1().getNom().equals("plante") && atk.getType2().getNom().equals("electrik")) || (atk.getType1().getNom().equals("electrik") && atk.getType2().getNom().equals("plante")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientplanteelectrique));
        }
        if((atk.getType1().getNom().equals("fee") && atk.getType2().getNom().equals("roche")) || (atk.getType1().getNom().equals("roche") && atk.getType2().getNom().equals("fee")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientfeeroche));
        }
        if((atk.getType1().getNom().equals("eau") && atk.getType2().getNom().equals("feu")) || (atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("eau")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradienteaufeu));
        }
        if((atk.getType1().getNom().equals("vol") && atk.getType2().getNom().equals("combat")) || (atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("vol")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientvolcombat));
        }
        if((atk.getType1().getNom().equals("psy") && atk.getType2().getNom().equals("tenebre")) || (atk.getType1().getNom().equals("tenebre") && atk.getType2().getNom().equals("psy")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientpsytenebre));
        }
        if((atk.getType1().getNom().equals("acier") && atk.getType2().getNom().equals("combat")) || (atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("acier")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientaciercombat));
        }
        if((atk.getType1().getNom().equals("eau") && atk.getType2().getNom().equals("normal")) || (atk.getType1().getNom().equals("normal") && atk.getType2().getNom().equals("eau")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradienteaunormal));
        }
        if((atk.getType1().getNom().equals("psy") && atk.getType2().getNom().equals("normal")) || (atk.getType1().getNom().equals("normal") && atk.getType2().getNom().equals("psy")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientpsynormal));
        }
        if((atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("glace")) || (atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("glace")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientfeuglace));
        }
        if((atk.getType1().getNom().equals("fee") && atk.getType2().getNom().equals("plante")) || (atk.getType1().getNom().equals("plante") && atk.getType2().getNom().equals("fee")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientfeeplante));
        }
        if((atk.getType1().getNom().equals("eau") && atk.getType2().getNom().equals("acier")) || (atk.getType1().getNom().equals("acier") && atk.getType2().getNom().equals("eau")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradienteauacier));
        }
        if((atk.getType1().getNom().equals("glace") && atk.getType2().getNom().equals("combat")) || (atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("glace")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientglacecombat));
        }
        if((atk.getType1().getNom().equals("dragon") && atk.getType2().getNom().equals("fee")) || (atk.getType1().getNom().equals("fee") && atk.getType2().getNom().equals("dragon")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientfeedragon));
        }
        if((atk.getType1().getNom().equals("spectre") && atk.getType2().getNom().equals("psy")) || (atk.getType1().getNom().equals("psy") && atk.getType2().getNom().equals("spectre")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientpsyspectre));
        }
        if((atk.getType1().getNom().equals("insecte") && atk.getType2().getNom().equals("spectre")) || (atk.getType1().getNom().equals("spectre") && atk.getType2().getNom().equals("insecte")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientinsectespectre));
        }
        if((atk.getType1().getNom().equals("dragon") && atk.getType2().getNom().equals("electrik")) || (atk.getType1().getNom().equals("electrik") && atk.getType2().getNom().equals("dragon")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientelectriquedragon));
        }
        if((atk.getType1().getNom().equals("tenebre") && atk.getType2().getNom().equals("feu")) || (atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("tenebre")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradienttenebrefeu));
        }
        if((atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("eau")) || (atk.getType1().getNom().equals("eau") && atk.getType2().getNom().equals("combat")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradienteaucombat));
        }
        if((atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("feu")) || (atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("combat")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientfeucombat));
        }
        if((atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("feu")) || (atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("combat")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientfeucombat));
        }
        if((atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("poison")) || (atk.getType1().getNom().equals("poison") && atk.getType2().getNom().equals("combat")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientpoisoncombat));
        }
        if((atk.getType1().getNom().equals("vol") && atk.getType2().getNom().equals("poison")) || (atk.getType1().getNom().equals("poison") && atk.getType2().getNom().equals("vol")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientpoisonvol));
        }
        if((atk.getType1().getNom().equals("sol") && atk.getType2().getNom().equals("roche")) || (atk.getType1().getNom().equals("roche") && atk.getType2().getNom().equals("sol")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientrochesol));
        }
        if((atk.getType1().getNom().equals("sol") && atk.getType2().getNom().equals("insecte")) || (atk.getType1().getNom().equals("insecte") && atk.getType2().getNom().equals("sol")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientinsectesol));
        }
        if((atk.getType1().getNom().equals("vol") && atk.getType2().getNom().equals("plante")) || (atk.getType1().getNom().equals("plante") && atk.getType2().getNom().equals("vol")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientplantevol));
        }
        if((atk.getType1().getNom().equals("roche") && atk.getType2().getNom().equals("psy")) || (atk.getType1().getNom().equals("psy") && atk.getType2().getNom().equals("roche")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientrochepsy));
        }
        if((atk.getType1().getNom().equals("roche") && atk.getType2().getNom().equals("feu")) || (atk.getType1().getNom().equals("feu") && atk.getType2().getNom().equals("roche")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientrochefeu));
        }
        if((atk.getType1().getNom().equals("normal") && atk.getType2().getNom().equals("combat")) || (atk.getType1().getNom().equals("combat") && atk.getType2().getNom().equals("normal")) ){
            b.setBackground(getResources().getDrawable(R.drawable.gradientnormalcombat));
        }

    }

}
