package com.example.pokemonfusion;

import java.io.Serializable;
import java.util.List;

class Equipe implements Serializable {
    private int id;
    private String pseudoUtilisateur, nom;
    private List<Pokemon> listePokemons;

    public Equipe(){}
    public Equipe(int id, String utilisateur, String nom, List<Pokemon> pokemons){
        this.id = id;
        this.nom = nom;
        this.pseudoUtilisateur = utilisateur;
        this.listePokemons = pokemons;
    }

    public int getId(){return this.id;}
    public String getPseudoUtilisateur() {
        return pseudoUtilisateur;
    }

    public String getNom(){return this.nom;}

    public List<Pokemon> getListePokemons() {
        return listePokemons;
    }

    public void setListePokemons(List<Pokemon> listePokemons) {
        this.listePokemons = listePokemons;
    }
}
