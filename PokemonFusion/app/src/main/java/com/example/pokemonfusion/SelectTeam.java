package com.example.pokemonfusion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.List;

public class SelectTeam extends AppCompatActivity {
    //private MediaPlayer mediaPlayer;
    private DBOpenHelper dbOpenHelper;
    private int idUt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selecteam);
        // Ouverture de la base de données
        dbOpenHelper = new DBOpenHelper(this);
        try {
            dbOpenHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            dbOpenHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }
        Intent intent = getIntent();
        this.idUt = intent.getIntExtra("idUt", 1);
        List<Equipe> listeEquipe = dbOpenHelper.getEquipeByUtilisateur(this.idUt);
        Button bouton = findViewById(R.id.button27);
        bouton.setText(listeEquipe.get(0).getNom());
        bouton.setContentDescription(listeEquipe.get(0).getId()+"");
    }

    public void retour(View view){
        Intent intentrecup = getIntent();
        String teamSelect = intentrecup.getStringExtra("teamselecte");
        Intent intent = new Intent(this, Acc.class);
        intent.putExtra("idUt", this.idUt);
        intent.putExtra("teamselecte",teamSelect);
        startActivity(intent);
    }

    public void selectionner(View view){
        Intent intentrecup = getIntent();
        String teamSelect = intentrecup.getStringExtra("teamselecte");
        String val = (String)view.getContentDescription();
        int idteam = Integer.parseInt(val.trim());
        dbOpenHelper.setPkmEquipe(idteam);

        Intent intent = new Intent(this, ModifTeam.class);
        intent.putExtra("idTeam",val);
        intent.putExtra("idUt", this.idUt);
        intent.putExtra("teamselecte",teamSelect);
        startActivity(intent);
    }

}
