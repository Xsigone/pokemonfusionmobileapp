package com.example.pokemonfusion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Hashtable;

public class Acc extends AppCompatActivity {
    //private MediaPlayer mediaPlayer;
    private DBOpenHelper dbOpenHelper;
    private int idUt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accueil);
        // Ouverture de la base de données
        dbOpenHelper = new DBOpenHelper(this);
        try {
            dbOpenHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            dbOpenHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }
        Intent intent = getIntent();
        this.idUt = intent.getIntExtra("idUt", 1);

    }

    public void combatre(View view){
        Intent intentrecup = getIntent();
        String teamSelect = intentrecup.getStringExtra("teamselecte");
        if(teamSelect.equals("aucune")){
            Toast toast = Toast.makeText(getApplicationContext(), "Vous devez selectionner une team pour combattre",Toast.LENGTH_SHORT);
            toast.show();
        }
        else{
            Intent intent = new Intent(this, Combat.class);
            int idteam = Integer.parseInt(teamSelect.trim());
            Equipe equipe = dbOpenHelper.getEquipeById(idteam);
            this.dbOpenHelper.setPkmEquipe(3);
            Equipe equipeA = dbOpenHelper.getEquipeById(3);
            GestionCombat combat = new GestionCombat(equipe, equipeA);
            intent.putExtra("combat",combat);
            startActivity(intent);}
    }
    public void deco(View view){
        Intent intent = new Intent(this, Pageconnexion.class);
        startActivity(intent);
    }
    public void selectTeam(View view){
        Intent intentrecup = getIntent();
        String teamSelect = intentrecup.getStringExtra("teamselecte");
        Intent intent = new Intent(this, SelectTeam.class);
        intent.putExtra("idUt",this.idUt);
        intent.putExtra("teamselecte",teamSelect);
        startActivity(intent);
    }



}
