package com.example.pokemonfusion;

import java.io.Serializable;
import java.util.Random;

public class GestionCombat implements Serializable {
    private Equipe equipe1;
    private Equipe equipe2;
    private Pokemon pokemonEquipe1;
    private Pokemon pokemonEquipe2;

    public GestionCombat(Equipe equipe1, Equipe equipe2){
        this.equipe1 = equipe1;
        this.equipe2 = equipe2;
        this.pokemonEquipe1 = equipe1.getListePokemons().get(0);
        this.pokemonEquipe2 = equipe2.getListePokemons().get(0);
    }

    public Equipe getEquipe1() {
        return equipe1;
    }

    public Equipe getEquipe2() {
        return equipe2;
    }

    public Pokemon getPokemonEquipe1() {
        return pokemonEquipe1;
    }

    public Pokemon getPokemonEquipe2() {
        return pokemonEquipe2;
    }

    public void setPokemonEquipe1(Pokemon pokemonEquipe1) {
        this.pokemonEquipe1 = pokemonEquipe1;
    }

    public void setPokemonEquipe2(Pokemon pokemonEquipe2) {
        this.pokemonEquipe2 = pokemonEquipe2;
    }

    public void gererStatut(){
        if (this.getPokemonEquipe2().getStatut().getDuree()!=999){
            this.getPokemonEquipe2().getStatut().diminuerDuree();
            if(this.getPokemonEquipe2().getStatut().getDuree()==0){
                this.getPokemonEquipe2().setStatus(new Statut(9, "NRM", 0, 999, 0, 0, 0));
            }
        }

        if (this.getPokemonEquipe1().getStatut().getDuree()!=999){
            this.getPokemonEquipe1().getStatut().diminuerDuree();
            if(this.getPokemonEquipe1().getStatut().getDuree()==0){
                this.getPokemonEquipe1().setStatus(new Statut(9, "NRM", 0, 999, 0, 0, 0));
            }
        }
    }

    public String attaqueAdverse(Attaque attaque){
        attaque.diminuePP();
        String puissance = "";
        Random random = new Random();
        int nb;
        nb = 1+random.nextInt(100-1);
        if(nb>this.getPokemonEquipe2().getStatut().getChanceBloquerAttaque()) {
            if (nb < attaque.getPrecision()) {
                if (attaque.getTypeAtk() == 1) {
                    float type = this.pokemonEquipe1.getType1().getType(attaque.getType1().getNom()) * this.pokemonEquipe1.getType2().getType(attaque.getType1().getNom()) * this.pokemonEquipe1.getType1().getType(attaque.getType2().getNom()) * this.pokemonEquipe1.getType2().getType(attaque.getType2().getNom());
                    if (type >= 4.0f) {
                        type = 4;
                        puissance = "c'est extremement efficace !! ";
                    }
                    if (type == 2.0f) {
                        puissance = "c'est super efficace ! ";
                    }
                    if (type == 0.5f) {
                        puissance = "ce n'est pas très efficace ! ";
                    }
                    if (type <= 0.25f && type != 0.0f) {
                        type = 0.25f;
                        puissance = "ce n'est absolument pas efficace !! ";
                    }
                    if (type == 0.0f) {
                        puissance = "cela n'a aucun effet !!! ";
                    }
                    float cm = this.pokemonEquipe2.getObjet().getAugmentePhysAtk() * type;
                    if (this.pokemonEquipe2.getType1().getNom().equals(attaque.getType1().getNom()) || this.pokemonEquipe2.getType2().getNom().equals(attaque.getType1().getNom()) || this.pokemonEquipe2.getType1().getNom().equals(attaque.getType2().getNom()) || this.pokemonEquipe2.getType2().getNom().equals(attaque.getType2().getNom())) {
                        if (this.pokemonEquipe2.getTalent().getNouveauxStab() == 0) {
                            cm *= 1.5;
                        } else {
                            cm *= this.pokemonEquipe2.getTalent().getNouveauxStab();
                        }
                    }
                    int degats = (int) (((((this.pokemonEquipe2.getStat().getAttaque() * this.pokemonEquipe2.getBoost().getAttaque()) * attaque.getPuissance()) / (this.pokemonEquipe1.getStat().getDefense() * this.pokemonEquipe1.getBoost().getDefense()) * 50) * cm) / 100);
                    this.pokemonEquipe1.subirDegats(degats);
                    if (attaque.getContreCoup() != 0) {
                        this.pokemonEquipe2.subirDegats(degats / attaque.getContreCoup());
                        puissance += "le pokemon subit des degats de recul ! ";
                    }
                } else if (attaque.getTypeAtk() == 2) {
                    float type = this.pokemonEquipe1.getType1().getType(attaque.getType1().getNom()) * this.pokemonEquipe1.getType2().getType(attaque.getType1().getNom()) * this.pokemonEquipe1.getType1().getType(attaque.getType2().getNom()) * this.pokemonEquipe1.getType2().getType(attaque.getType2().getNom());
                    if (type >= 4.0f) {
                        type = 4;
                        puissance = "c'est extremement efficace !! ";
                    }
                    if (type == 2.0f) {
                        puissance = "c'est super efficace ! ";
                    }
                    if (type == 0.5f) {
                        puissance = "ce n'est pas très efficace ! ";
                    }
                    if (type <= 0.25f && type != 0.0f) {
                        type = 0.25f;
                        puissance = "ce n'est absolument pas efficace !! ";
                    }
                    if (type == 0.0f) {
                        puissance = "cela n'a aucun effet !!! ";
                    }
                    float cm = this.pokemonEquipe2.getObjet().getAugmenteSpeAtk() * type;
                    if (this.pokemonEquipe2.getType1().getNom().equals(attaque.getType1().getNom()) || this.pokemonEquipe2.getType2().getNom().equals(attaque.getType1().getNom()) || this.pokemonEquipe2.getType1().getNom().equals(attaque.getType2().getNom()) || this.pokemonEquipe2.getType2().getNom().equals(attaque.getType2().getNom())) {
                        if (this.pokemonEquipe2.getTalent().getNouveauxStab() == 0) {
                            cm *= 1.5;
                        } else {
                            cm *= this.pokemonEquipe2.getTalent().getNouveauxStab();
                        }
                    }
                    int degats = (int) (((((this.pokemonEquipe2.getStat().getAttaqueSpe() * this.pokemonEquipe2.getBoost().getAttaqueSpe()) * attaque.getPuissance()) / (this.pokemonEquipe1.getStat().getDefenseSpe() * this.pokemonEquipe1.getBoost().getDefenseSpe()) * 50) * cm) / 100);
                    this.pokemonEquipe1.subirDegats(degats);
                    if (attaque.getContreCoup() != 0) {
                        this.pokemonEquipe2.subirDegats(degats / attaque.getContreCoup());
                        puissance += "le pokemon subit des degats de recul ! ";
                    }
                }
                float boostAtk = attaque.getChangeAtkPhys();
                if (boostAtk > 1) {
                    if (this.pokemonEquipe2.getBoost().getAttaque() >= 4.0) {
                        boostAtk = 1;
                        puissance += "L'attaque du " + this.pokemonEquipe2.getNom() + " est deja au max. ";
                    } else {
                        puissance += "L'attaque du " + this.pokemonEquipe2.getNom() + " augmente. ";
                    }
                }
                if (boostAtk < 1) {
                    if (this.pokemonEquipe2.getBoost().getAttaque() <= 0.25) {
                        boostAtk = 1;
                        puissance += "L'attaque du " + this.pokemonEquipe2.getNom() + " est deja au min. ";
                    } else {
                        puissance += "L'attaque du " + this.pokemonEquipe2.getNom() + " diminue. ";
                    }
                }
                float boostDef = attaque.getChangeDefPhys();
                if (boostDef > 1) {
                    if (this.pokemonEquipe2.getBoost().getDefense() >= 4.0) {
                        boostDef = 1;
                        puissance += "La defense du " + this.pokemonEquipe2.getNom() + " est deja au max. ";
                    } else {
                        puissance += "La defense du " + this.pokemonEquipe2.getNom() + " augmente. ";
                    }

                }
                if (boostDef < 1) {
                    if (this.pokemonEquipe2.getBoost().getDefense() <= 0.25) {
                        boostDef = 1;
                        puissance += "La defense du " + this.pokemonEquipe2.getNom() + " est deja au min. ";
                    } else {
                        puissance += "La defense du " + this.pokemonEquipe2.getNom() + " diminue. ";
                    }
                }
                float boostAtkSpe = attaque.getChangeAtkSpe();
                if (boostAtkSpe > 1) {
                    if (this.pokemonEquipe2.getBoost().getAttaqueSpe() >= 4.0) {
                        boostAtkSpe = 1;
                        puissance += "L'attaque spéciale du " + this.pokemonEquipe2.getNom() + " est deja au max. ";
                    } else {
                        puissance += "L'attaque spéciale du " + this.pokemonEquipe2.getNom() + " augmente. ";
                    }
                }
                if (boostAtkSpe < 1) {
                    if (this.pokemonEquipe2.getBoost().getAttaqueSpe() <= 0.25) {
                        boostAtkSpe = 1;
                        puissance += "L'attaque spéciale du " + this.pokemonEquipe2.getNom() + " est deja au min. ";
                    } else {
                        puissance += "L'attaque spéciale du " + this.pokemonEquipe2.getNom() + " diminue. ";
                    }
                }
                float boostDefSpe = attaque.getChangeDefSpe();
                if (boostDefSpe > 1) {
                    if (this.pokemonEquipe2.getBoost().getDefenseSpe() >= 4.0) {
                        boostDefSpe = 1;
                        puissance += "La defense spéciale du " + this.pokemonEquipe2.getNom() + " est deja au max. ";
                    } else {
                        puissance += "La defense spéciale du " + this.pokemonEquipe2.getNom() + " augmente. ";
                    }
                }
                if (boostDefSpe < 1) {
                    if (this.pokemonEquipe2.getBoost().getDefenseSpe() <= 0.25) {
                        boostDefSpe = 1;
                        puissance += "La defense spéciale du " + this.pokemonEquipe2.getNom() + " est deja au min. ";
                    } else {
                        puissance += "La defense spéciale du " + this.pokemonEquipe2.getNom() + " diminue. ";
                    }
                }
                float boostVitesse = attaque.getChangeSpeed();
                if (boostVitesse > 1) {
                    if (this.pokemonEquipe2.getBoost().getVitesse() >= 4.0) {
                        boostVitesse = 1;
                        puissance += "La vitesse du " + this.pokemonEquipe2.getNom() + " est deja au max. ";
                    } else {
                        puissance += "La vitesse du " + this.pokemonEquipe2.getNom() + " augmente. ";
                    }
                }
                if (boostVitesse < 1) {
                    if (this.pokemonEquipe2.getBoost().getVitesse() <= 0.25) {
                        boostVitesse = 1;
                        puissance += "La vitesse du " + this.pokemonEquipe2.getNom() + " est deja au min. ";
                    } else {
                        puissance += "La vitesse du " + this.pokemonEquipe2.getNom() + " diminue. ";
                    }

                }


                pokemonEquipe2.getBoost().ajoute(new Stat(1, boostAtk, boostDef, boostAtkSpe, boostDefSpe, boostVitesse));

                Random random1 = new Random();
                int nb1;
                nb1 = 1 + random1.nextInt(100 - 1);
                if (nb < attaque.getChanceDonneStatut()) {
                    this.getPokemonEquipe1().setStatus(attaque.getStatutEnnemie());
                    this.getPokemonEquipe2().setStatus(attaque.getStatutLanceur());
                }

                if (attaque.getVieDonnee() > 0) {
                    this.getPokemonEquipe2().soigne(this.pokemonEquipe2.getStat().getPv() / attaque.getVieDonnee());
                    puissance += "le pokemon recupere de la vie ! ";
                }
            } else {
                puissance += "l'attaque a échoué ";
            }
        }else{
            puissance += "le pokemon n'a pas pu attaquer ";
            if (this.getPokemonEquipe2().getStatut().getNom().equals("CON")){
                this.getPokemonEquipe2().subirDegats(this.pokemonEquipe2.getStat().getPv()/this.pokemonEquipe2.getStatut().getDegats());
                puissance+="il se blesse dans sa confusion ! ";
            }
        }
        if (!this.getPokemonEquipe2().getStatut().getNom().equals("CON")){
            if (this.pokemonEquipe2.getStatut().getDegats()!=0) {
                this.getPokemonEquipe2().subirDegats(this.pokemonEquipe2.getStat().getPv() / this.pokemonEquipe2.getStatut().getDegats());
                puissance += "le pokemon subit des degats ! ";
                this.getPokemonEquipe2().getStatut().augmenterDegat();
            }
            Random random1 = new Random();
            int nb1;
            nb1 = 1 + random1.nextInt(100 - 1);
            if (nb1<this.getPokemonEquipe2().getStatut().getChanceEnleve()){
                this.getPokemonEquipe2().setStatus(new Statut(9, "NRM", 0, 999, 0, 0, 0));
                puissance+="le statut du pokemon reviens a la normal ";
            }


        }
        return puissance;
    }

    public String attaqueJoueur(Attaque attaque){
        String puissance = "";
        Random random = new Random();
        int nb;
        nb = 1+random.nextInt(100-1);
        attaque.diminuePP();
        if(nb>this.getPokemonEquipe1().getStatut().getChanceBloquerAttaque()) {
            if (nb < attaque.getPrecision()) {
                if (attaque.getTypeAtk() == 1) {
                    float type = this.pokemonEquipe2.getType1().getType(attaque.getType1().getNom()) * this.pokemonEquipe2.getType2().getType(attaque.getType1().getNom()) * this.pokemonEquipe2.getType1().getType(attaque.getType2().getNom()) * this.pokemonEquipe2.getType2().getType(attaque.getType2().getNom());
                    if (type >= 4.0f) {
                        type = 4;
                        puissance = "c'est extremement efficace !! ";
                    }
                    if (type == 2.0f) {
                        puissance = "c'est super efficace ! ";
                    }
                    if (type == 0.5f) {
                        puissance = "ce n'est pas très efficace ! ";
                    }
                    if (type <= 0.25f && type != 0.0f) {
                        type = 0.25f;
                        puissance = "ce n'est absolument pas efficace !! ";
                    }
                    if (type == 0.0f) {
                        puissance = "cela n'a aucun effet !!! ";
                    }
                    float cm = this.pokemonEquipe1.getObjet().getAugmentePhysAtk() * type;
                    if (this.pokemonEquipe1.getType1().getNom().equals(attaque.getType1().getNom()) || this.pokemonEquipe1.getType2().getNom().equals(attaque.getType1().getNom()) || this.pokemonEquipe1.getType1().getNom().equals(attaque.getType2().getNom()) || this.pokemonEquipe1.getType2().getNom().equals(attaque.getType2().getNom())) {
                        if (this.pokemonEquipe1.getTalent().getNouveauxStab() == 0) {
                            cm *= 1.5;
                        } else {
                            cm *= this.pokemonEquipe1.getTalent().getNouveauxStab();
                        }
                    }
                    int degats = (int) (((((this.pokemonEquipe1.getStat().getAttaque() * this.pokemonEquipe1.getBoost().getAttaque()) * attaque.getPuissance()) / (this.pokemonEquipe2.getStat().getDefense() * this.pokemonEquipe2.getBoost().getDefense()) * 50) * cm) / 100);
                    this.pokemonEquipe2.subirDegats(degats);
                    if (attaque.getContreCoup() != 0) {
                        this.pokemonEquipe1.subirDegats(degats / attaque.getContreCoup());
                        puissance += "le pokemon subit des degats de recul ! ";
                    }
                } else if (attaque.getTypeAtk() == 2) {
                    float type = this.pokemonEquipe2.getType1().getType(attaque.getType1().getNom()) * this.pokemonEquipe2.getType2().getType(attaque.getType1().getNom()) * this.pokemonEquipe2.getType1().getType(attaque.getType2().getNom()) * this.pokemonEquipe2.getType2().getType(attaque.getType2().getNom());
                    if (type >= 4.0f) {
                        type = 4;
                        puissance = "c'est extremement efficace !! ";
                    }
                    if (type == 2.0f) {
                        puissance = "c'est super efficace ! ";
                    }
                    if (type == 0.5f) {
                        puissance = "ce n'est pas très efficace ! ";
                    }
                    if (type <= 0.25f && type != 0.0f) {
                        type = 0.25f;
                        puissance = "ce n'est absolument pas efficace !! ";
                    }
                    if (type == 0.0f) {
                        puissance = "cela n'a aucun effet !!! ";
                    }
                    float cm = this.pokemonEquipe1.getObjet().getAugmenteSpeAtk() * type;
                    if (this.pokemonEquipe1.getType1().getNom().equals(attaque.getType1().getNom()) || this.pokemonEquipe1.getType2().getNom().equals(attaque.getType1().getNom()) || this.pokemonEquipe1.getType1().getNom().equals(attaque.getType2().getNom()) || this.pokemonEquipe1.getType2().getNom().equals(attaque.getType2().getNom())) {
                        if (this.pokemonEquipe1.getTalent().getNouveauxStab() == 0) {
                            cm *= 1.5;
                        } else {
                            cm *= this.pokemonEquipe1.getTalent().getNouveauxStab();
                        }
                    }
                    int degats = (int) (((((this.pokemonEquipe1.getStat().getAttaqueSpe() * this.pokemonEquipe1.getBoost().getAttaqueSpe()) * attaque.getPuissance()) / (this.pokemonEquipe2.getStat().getDefenseSpe() * this.pokemonEquipe2.getBoost().getDefenseSpe()) * 50) * cm) / 100);
                    this.pokemonEquipe2.subirDegats(degats);
                    if (attaque.getContreCoup() != 0) {
                        this.pokemonEquipe1.subirDegats(degats / attaque.getContreCoup());
                        puissance += "le pokemon subit des degats de recul ! ";
                    }
                }
                float boostAtk = attaque.getChangeAtkPhys();
                if (boostAtk > 1) {
                    if (this.pokemonEquipe1.getBoost().getAttaque() >= 4.0) {
                        boostAtk = 1;
                        puissance += "L'attaque du " + this.pokemonEquipe1.getNom() + " est deja au max. ";
                    } else {
                        puissance += "L'attaque du " + this.pokemonEquipe1.getNom() + " augmente. ";
                    }
                }
                if (boostAtk < 1) {
                    if (this.pokemonEquipe1.getBoost().getAttaque() <= 0.25) {
                        boostAtk = 1;
                        puissance += "L'attaque du " + this.pokemonEquipe1.getNom() + " est deja au min. ";
                    } else {
                        puissance += "L'attaque du " + this.pokemonEquipe1.getNom() + " diminue. ";
                    }
                }
                float boostDef = attaque.getChangeDefPhys();
                if (boostDef > 1) {
                    if (this.pokemonEquipe1.getBoost().getDefense() >= 4.0) {
                        boostDef = 1;
                        puissance += "La defense du " + this.pokemonEquipe1.getNom() + " est deja au max. ";
                    } else {
                        puissance += "La defense du " + this.pokemonEquipe1.getNom() + " augmente. ";
                    }

                }
                if (boostDef < 1) {
                    if (this.pokemonEquipe1.getBoost().getDefense() <= 0.25) {
                        boostDef = 1;
                        puissance += "La defense du " + this.pokemonEquipe1.getNom() + " est deja au min. ";
                    } else {
                        puissance += "La defense du " + this.pokemonEquipe1.getNom() + " diminue. ";
                    }
                }
                float boostAtkSpe = attaque.getChangeAtkSpe();
                if (boostAtkSpe > 1) {
                    if (this.pokemonEquipe1.getBoost().getAttaqueSpe() >= 4.0) {
                        boostAtkSpe = 1;
                        puissance += "L'attaque spéciale du " + this.pokemonEquipe1.getNom() + " est deja au max. ";
                    } else {
                        puissance += "L'attaque spéciale du " + this.pokemonEquipe1.getNom() + " augmente. ";
                    }
                }
                if (boostAtkSpe < 1) {
                    if (this.pokemonEquipe1.getBoost().getAttaqueSpe() <= 0.25) {
                        boostAtkSpe = 1;
                        puissance += "L'attaque spéciale du " + this.pokemonEquipe1.getNom() + " est deja au min. ";
                    } else {
                        puissance += "L'attaque spéciale du " + this.pokemonEquipe1.getNom() + " diminue. ";
                    }
                }
                float boostDefSpe = attaque.getChangeDefSpe();
                if (boostDefSpe > 1) {
                    if (this.pokemonEquipe1.getBoost().getDefenseSpe() >= 4.0) {
                        boostDefSpe = 1;
                        puissance += "La defense spéciale du " + this.pokemonEquipe1.getNom() + " est deja au max. ";
                    } else {
                        puissance += "La defense spéciale du " + this.pokemonEquipe1.getNom() + " augmente. ";
                    }
                }
                if (boostDefSpe < 1) {
                    if (this.pokemonEquipe1.getBoost().getDefenseSpe() <= 0.25) {
                        boostDefSpe = 1;
                        puissance += "La defense spéciale du " + this.pokemonEquipe1.getNom() + " est deja au min. ";
                    } else {
                        puissance += "La defense spéciale du " + this.pokemonEquipe1.getNom() + " diminue. ";
                    }
                }
                float boostVitesse = attaque.getChangeSpeed();
                if (boostVitesse > 1) {
                    if (this.pokemonEquipe1.getBoost().getVitesse() >= 4.0) {
                        boostVitesse = 1;
                        puissance += "La vitesse du " + this.pokemonEquipe1.getNom() + " est deja au max. ";
                    } else {
                        puissance += "La vitesse du " + this.pokemonEquipe1.getNom() + " augmente. ";
                    }
                }
                if (boostVitesse < 1) {
                    if (this.pokemonEquipe1.getBoost().getVitesse() <= 0.25) {
                        boostVitesse = 1;
                        puissance += "La vitesse du " + this.pokemonEquipe1.getNom() + " est deja au min. ";
                    } else {
                        puissance += "La vitesse du " + this.pokemonEquipe1.getNom() + " diminue. ";
                    }

                }


                pokemonEquipe1.getBoost().ajoute(new Stat(1, boostAtk, boostDef, boostAtkSpe, boostDefSpe, boostVitesse));

                Random random1 = new Random();
                int nb1;
                nb1 = 1 + random1.nextInt(100 - 1);
                if (nb < attaque.getChanceDonneStatut() && attaque.getStatutEnnemie().getId() != 9) {
                    puissance += "un des statuts a été modifié ! ";
                    this.getPokemonEquipe2().setStatus(attaque.getStatutEnnemie());
                    this.getPokemonEquipe1().setStatus(attaque.getStatutLanceur());
                }

                if (attaque.getVieDonnee() > 0) {
                    this.getPokemonEquipe1().soigne(this.pokemonEquipe1.getStat().getPv() / attaque.getVieDonnee());
                    puissance += "le pokemon recupere de la vie ! ";
                }
            } else {
                puissance += "l'attaque a échoué";
            }
        }else{
            puissance += "le pokemon n'a pas pu attaquer ";
            if (this.getPokemonEquipe1().getStatut().getNom().equals("CON")){
                this.getPokemonEquipe1().subirDegats(this.pokemonEquipe1.getStat().getPv()/this.pokemonEquipe1.getStatut().getDegats());
                puissance+="il se blesse dans sa confusion ! ";
            }
        }
        if (!this.getPokemonEquipe1().getStatut().getNom().equals("CON")){
            if(this.pokemonEquipe1.getStatut().getDegats()!=0) {
                this.getPokemonEquipe1().subirDegats(this.pokemonEquipe1.getStat().getPv() / this.pokemonEquipe1.getStatut().getDegats());
                puissance += "le pokemon subit des degats ! ";
                this.getPokemonEquipe1().getStatut().augmenterDegat();
            }
            Random random1 = new Random();
            int nb1;
            nb1 = 1 + random1.nextInt(100 - 1);
            if (nb1<this.getPokemonEquipe1().getStatut().getChanceEnleve()){
                this.getPokemonEquipe1().setStatus(new Statut(9, "NRM", 0, 999, 0, 0, 0));
                puissance+="le statut du pokemon reviens a la normal ";
            }


        }
        return puissance;
    }

    public void switchAdverse(){
        for (Pokemon pok : this.equipe2.getListePokemons()){
            if (!pok.isMort()){
                this.pokemonEquipe2 = pok;
            }
        }
    }
}
