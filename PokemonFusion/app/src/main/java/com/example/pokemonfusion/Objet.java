package com.example.pokemonfusion;

import java.io.Serializable;

class Objet implements Serializable {

    int id;
    String nom;
    float augmenteSpeAtk, augmentePhysAtk, augmenteVite;
    int regenVie, contrecoup, condVieProc, resistance1hp;

    public Objet() {
    }

    public Objet(int id, String nom, float augmenteSpeAtk, float augmentePhysAtk, float augmenteVite, int regenVie, int contrecoup, int condVieProc, int resistance1hp) {
        this.id = id;
        this.nom = nom;
        this.augmenteSpeAtk = augmenteSpeAtk;
        this.augmentePhysAtk = augmentePhysAtk;
        this.augmenteVite = augmenteVite;
        this.regenVie = regenVie;
        this.contrecoup = contrecoup;
        this.condVieProc = condVieProc;
        this.resistance1hp = resistance1hp;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public float getAugmenteSpeAtk() {
        return augmenteSpeAtk;
    }

    public float getAugmentePhysAtk() {
        return augmentePhysAtk;
    }

    public float getAugmenteVite() {
        return augmenteVite;
    }

    public int getRegenVie() {
        return regenVie;
    }

    public int getContrecoup() {
        return contrecoup;
    }

    public int getCondVieProc() {
        return condVieProc;
    }

    public int getResistance1hp() {
        return resistance1hp;
    }
}
