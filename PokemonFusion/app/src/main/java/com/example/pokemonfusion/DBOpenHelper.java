package com.example.pokemonfusion;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;


public class DBOpenHelper extends SQLiteOpenHelper {

    String DB_PATH = null;
    private static String DB_NAME = "pokemonfusionDB";
    private SQLiteDatabase myDataBase;
    private final Context myContext;

    public DBOpenHelper(Context context) {
        super(context, DB_NAME, null, 10);
        this.myContext = context;
        this.DB_PATH = "/data/data/" + context.getPackageName() + "/" + "databases/";
        Log.e("Path 1", DB_PATH);
    }


    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();
        if (dbExist) {
        } else {
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        } catch (SQLiteException e) {
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }

    private void copyDataBase() throws IOException {
        InputStream myInput = myContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[10];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public void openDataBase() throws SQLException {
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

    }

    @Override
    public synchronized void close() {
        if (myDataBase != null)
            myDataBase.close();
        super.close();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion)
            try {
                copyDataBase();
            } catch (IOException e) {
                e.printStackTrace();

            }
    }

    public void ajouterUtilisateur(String pseudo, String mdp) {
        ContentValues content = new ContentValues();

        content.put("pseudo", pseudo);
        content.put("mdp", mdp);

        myDataBase.insert("Utilisateur", null, content);
        List<Integer> listePokemon = new ArrayList<>();
        List<Integer> listetousidpokemon = getListePokemon();
        int var1 = genererInt(1,listetousidpokemon.size());
        int idpkm1 = listetousidpokemon.get(var1);
        listetousidpokemon.remove(var1);
        listePokemon.add(idpkm1);
        int var2 = genererInt(1,listetousidpokemon.size());
        int idpkm2 = listetousidpokemon.get(var2);
        listetousidpokemon.remove(var2);
        listePokemon.add(idpkm2);
        int var3 = genererInt(1,listetousidpokemon.size());
        int idpkm3 = listetousidpokemon.get(var3);
        listetousidpokemon.remove(var3);
        listePokemon.add(idpkm3);
        int var4 = genererInt(1,listetousidpokemon.size());
        int idpkm4 = listetousidpokemon.get(var4);
        listetousidpokemon.remove(var4);
        listePokemon.add(idpkm4);
        int var5 = genererInt(1,listetousidpokemon.size());
        int idpkm5 = listetousidpokemon.get(var5);
        listetousidpokemon.remove(var5);
        listePokemon.add(idpkm5);
        int var6 = genererInt(1,listetousidpokemon.size());
        int idpkm6 = listetousidpokemon.get(var6);
        listetousidpokemon.remove(var6);
        listePokemon.add(idpkm6);
        ajouterEquipe(getIdByPseudo(pseudo), listePokemon);
    }

    public void ajouterEquipe(int idUtilisateur, List<Integer> listePokemon) {
        ContentValues content = new ContentValues();

        content.put("utilisateur", idUtilisateur);
        content.put("nom", "team");
        for (int i=0; i<listePokemon.size(); i++){
            content.put("pok"+(i+1), listePokemon.get(i));
        }

        myDataBase.insert("Equipe", null, content);
    }


    int genererInt(int borneInf, int borneSup){
        Random random = new Random();
        int nb;
        nb = borneInf+random.nextInt(borneSup-borneInf);
        return nb;
    }

    public void setPkmEquipe(int id){
        Equipe equipe = getEquipeById(id);
        List<Integer> listetousidpokemon = getListePokemon();
        int var1 = genererInt(1,listetousidpokemon.size());
        int idpkm1 = listetousidpokemon.get(var1);
        listetousidpokemon.remove(var1);
        int var2 = genererInt(1,listetousidpokemon.size());
        int idpkm2 = listetousidpokemon.get(var2);
        listetousidpokemon.remove(var2);
        int var3 = genererInt(1,listetousidpokemon.size());
        int idpkm3 = listetousidpokemon.get(var3);
        listetousidpokemon.remove(var3);
        int var4 = genererInt(1,listetousidpokemon.size());
        int idpkm4 = listetousidpokemon.get(var4);
        listetousidpokemon.remove(var4);
        int var5 = genererInt(1,listetousidpokemon.size());
        int idpkm5 = listetousidpokemon.get(var5);
        listetousidpokemon.remove(var5);
        int var6 = genererInt(1,listetousidpokemon.size());
        int idpkm6 = listetousidpokemon.get(var6);
        listetousidpokemon.remove(var6);
        String strFilter = "id=" + id;
        ContentValues args = new ContentValues();
        args.put("pok1", idpkm1);
        args.put("pok2", idpkm2);
        args.put("pok3", idpkm3);
        args.put("pok4", idpkm4);
        args.put("pok5", idpkm5);
        args.put("pok6", idpkm6);
        myDataBase.update("Equipe", args, strFilter, null);

    }

 /*   public void ajouterStat(String pseudo, String mdp) {
        ContentValues content = new ContentValues();

        content.put("pseudo", pseudo);
        content.put("mdp", mdp);

        myDataBase.insert("Utilisateur", null, content);
    }

    public void ajouterPokemon(Pokemon pokemon, List<Integer> listeAttaque, int idStatActuel, int idEVIV, int idObjet, int idTalent) {
        ContentValues content = new ContentValues();

        content.put("nom", pokemon.getNom());
        content.put("type1", pokemon.getType1().getNom());
        content.put("type2", pokemon.getType2().getNom());
        for (int i=0; i<listeAttaque.size(); i++){
            content.put("attaque"+(i+1), listeAttaque.get(i));
        }

        myDataBase.insert("Utilisateur", null, content);
    }
*/
    // ------------------------------------------  Requetes pour Utilisateur ---------------------------------------------------------------------

    public Hashtable<String, Utilisateur> getUtilisateurHash() {
        Hashtable<String, Utilisateur> utilisateurs = new Hashtable<>();
        String[] columns = {"id", "pseudo", "mdp"};
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Utilisateur", columns, null, null, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            utilisateurs.put(cursor.getString(1), new Utilisateur(cursor.getInt(0), cursor.getString(1), cursor.getString(2)));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return utilisateurs;
    }

    public Utilisateur getUtilisateurById(int id) {
        String[] columns = {"pseudo", "mdp"};
        Utilisateur utilisateur = new Utilisateur();
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Utilisateur", columns, "id=?", new String[]{"" + id}, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            utilisateur = new Utilisateur(id, cursor.getString(0), cursor.getString(1));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return utilisateur;
    }

    public String getPseudoUtById(int id) {
        String[] columns = {"pseudo"};
        String pseudo = "null";
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Utilisateur", columns, "id=?", new String[]{"" + id}, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            pseudo = cursor.getString(0);
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return pseudo;
    }

    public int getIdByPseudo(String pseudo) {
        String[] columns = {"id"};
        int id = 1;
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Utilisateur", columns, "pseudo=?", new String[]{pseudo}, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            id = cursor.getInt(0);
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return id;
    }


    public List<Utilisateur> getUtilisateur() {
        List<Utilisateur> utilisateurs = new ArrayList<>();
        String[] columns = {"id", "pseudo", "mdp"};
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Utilisateur", columns, null, null, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            utilisateurs.add(new Utilisateur(cursor.getInt(0), cursor.getString(1), cursor.getString(2)));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return utilisateurs;
    }

    // ------------------------------------------  Requetes pour Equipe ---------------------------------------------------------------------

    public List<Equipe> getEquipeByUtilisateur(int idUt){
        String[] columns = {"id", "nom", "pok1", "pok2", "pok3", "pok4", "pok5", "pok6"};
        List<Equipe> listeEquipe = new ArrayList<>();
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Equipe", columns, "utilisateur=?", new String[]{"" + idUt}, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            List<Pokemon> listePokemon = new ArrayList<>();
            // Récupération d'une chaîne et insertion dans une liste.
            listeEquipe.add(new Equipe(cursor.getInt(0), getPseudoUtById(idUt),cursor.getString(1) , listePokemon));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return listeEquipe;
    }

    public Equipe getEquipeById(int id){
        String[] columns = {"id", "utilisateur","nom", "pok1", "pok2", "pok3", "pok4", "pok5", "pok6"};
        Equipe equipe = new Equipe();
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Equipe", columns, "id=?", new String[]{id+""}, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            List<Pokemon> listePokemon = new ArrayList<>();
            listePokemon.add(getPokemonById(cursor.getInt(3)));
            listePokemon.add(getPokemonById(cursor.getInt(4)));
            listePokemon.add(getPokemonById(cursor.getInt(5)));
            listePokemon.add(getPokemonById(cursor.getInt(6)));
            listePokemon.add(getPokemonById(cursor.getInt(7)));
            listePokemon.add(getPokemonById(cursor.getInt(8)));
            // Récupération d'une chaîne et insertion dans une liste.
            equipe = new Equipe(id, getPseudoUtById(cursor.getInt(1)), cursor.getString(2), listePokemon);
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return equipe;
    }

    // ------------------------------------------  Requetes pour Pokemon ---------------------------------------------------------------------

    public List<Pokemon> getListePokemonBase() {
        List<Pokemon> pokemons = new ArrayList<>();
        String[] columns = {"id", "nom", "type1", "type2", "statBase"};
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("PokemonBase", columns, null, null, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            pokemons.add(new Pokemon(cursor.getInt(0), cursor.getString(1), getTypeById(cursor.getInt(2)), getTypeById(cursor.getInt(3)), getStatById(cursor.getInt(4))));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return pokemons;
    }
    public List<Integer> getListePokemon() {
        List<Integer> pokemons = new ArrayList<>();
        String[] columns = {"id"};
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Pokemon", columns, null, null, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            pokemons.add(cursor.getInt(0));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return pokemons;
    }

    public Pokemon getPokemonById(int id) {
        String[] columns = {"nom", "type1", "type2", "attaque1", "attaque2", "attaque3", "attaque4", "statBase", "EVIV", "objet", "talent", "statut"};
        Pokemon pokemon = new Pokemon();
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Pokemon", columns, "id=?", new String[]{"" + id}, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            Type type1 = getTypeById(cursor.getInt(1));
            Type type2 = getTypeById(cursor.getInt(2));
            Attaque attaque1 = getAttaqueById(cursor.getInt(3));
            Attaque attaque2 = getAttaqueById(cursor.getInt(4));
            Attaque attaque3 = getAttaqueById(cursor.getInt(5));
            Attaque attaque4 = getAttaqueById(cursor.getInt(6));

            Stat stat = getStatById(cursor.getInt(7));

            Objet objet = getObjetById(cursor.getInt(9));

            Talent talent = getTalentById(cursor.getInt(10));

            Statut statut = getStatutById(cursor.getInt(11));

            pokemon = new Pokemon(id, cursor.getString(0), type1, type2, stat, objet, talent, attaque1, attaque2, attaque3, attaque4, statut);
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return pokemon;
    }

    // ------------------------------------------  Requetes pour Type ---------------------------------------------------------------------

    public Type getTypeById(int id) {
        String[] columns = {"nom", "acier", "combat", "dragon", "eau", "electrik", "fee", "feu", "glace", "insecte", "normal", "plante", "poison", "psy", "roche", "sol", "spectre", "tenebre", "vol"};
        Type type = new Type();
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Type", columns, "id=?", new String[]{"" + id}, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            type = new Type(id, cursor.getString(0), cursor.getFloat(1), cursor.getFloat(2), cursor.getFloat(3), cursor.getFloat(4), cursor.getFloat(5), cursor.getFloat(6), cursor.getFloat(7), cursor.getFloat(8), cursor.getFloat(9), cursor.getFloat(10), cursor.getFloat(11), cursor.getFloat(12), cursor.getFloat(13), cursor.getFloat(14), cursor.getFloat(15), cursor.getFloat(16), cursor.getFloat(17), cursor.getFloat(18));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return type;
    }

    // ------------------------------------------  Requetes pour Attaque ---------------------------------------------------------------------

    public Attaque getAttaqueById(int id) {
        String[] columns = {"nom", "type1", "type2", "puissance", "precision", "statutPrEnnemie", "pp", "relance", "changeAtkPhys", "changeAtkSpe", "changeSpeed", "changeDefPhys", "changeDefSpe", "typeAtk", "vieDonnee", "chanceDonneStatut", "priorité", "contrecoup", "statutPrLanceur"};
        Attaque attaque = new Attaque();
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Attaque", columns, "id=?", new String[]{"" + id}, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            attaque = new Attaque(id, cursor.getString(0), getTypeById(cursor.getInt(1)), getTypeById(cursor.getInt(2)), cursor.getInt(3), cursor.getInt(4), cursor.getInt(6), cursor.getInt(7), cursor.getFloat(8), cursor.getFloat(9), cursor.getFloat(10), cursor.getFloat(11), cursor.getFloat(12), cursor.getInt(13), cursor.getInt(14), cursor.getInt(15), cursor.getInt(16), cursor.getInt(17), getStatutById(cursor.getInt(5)), getStatutById(cursor.getInt(18)));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return attaque;
    }

    // ------------------------------------------  Requetes pour Statut ---------------------------------------------------------------------

    public Statut getStatutById(int id) {
        String[] columns = {"nom", "degats", "duree", "chanceBloquerAttaque", "chanceEnleve", "augmentationDegats"};
        Statut statut = new Statut();
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Statut", columns, "id=?", new String[]{"" + id}, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            statut = new Statut(id, cursor.getString(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getInt(5));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return statut;
    }

    // ------------------------------------------  Requetes pour Stat ---------------------------------------------------------------------

    public Stat getStatById(int id) {
        String[] columns = {"pv", "attaque", "attaqueSpe", "defense", "defenseSpe", "vitesse"};
        Stat stat = new Stat();
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Stat", columns, "id=?", new String[]{"" + id}, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            stat = new Stat(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getInt(5));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return stat;
    }

    // ------------------------------------------  Requetes pour Objet ---------------------------------------------------------------------

    public Objet getObjetById(int id) {
        String[] columns = {"nom", "augmenteSpeAtk", "augmentePhysAtk", "augmenteVite", "regenVie", "contrecoup", "condVieProc", "resistance1hp"};
        Objet objet = new Objet();
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Objet", columns, "id=?", new String[]{"" + id}, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            objet = new Objet(id, cursor.getString(0), cursor.getFloat(1), cursor.getFloat(2), cursor.getFloat(3), cursor.getInt(4), cursor.getInt(5), cursor.getInt(6), cursor.getInt(7));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return objet;
    }

    // ------------------------------------------  Requetes pour Talent ---------------------------------------------------------------------

    public Talent getTalentById(int id) {
        String[] columns = {"nom", "immuElec", "immuEau", "regen", "condVie", "survie1Hp", "NouveauxStab", "augmenterVitesse"};
        Talent talent = new Talent();
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = myDataBase.query("Talent", columns, "id=?", new String[]{"" + id}, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            talent = new Talent(id, cursor.getString(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getInt(5), cursor.getInt(6), cursor.getInt(7));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return talent;
    }

}
