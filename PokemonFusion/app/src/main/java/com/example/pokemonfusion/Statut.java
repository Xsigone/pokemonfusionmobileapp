package com.example.pokemonfusion;

import java.io.Serializable;

class Statut implements Serializable {

    private int id;
    private String nom;
    private int degats, duree, chanceBloquerAttaque, chanceEnleve, augmentationDegats;


    public Statut() {
    }

    public Statut(int id, String nom, int degats, int duree, int chanceBloquerAttaque, int chanceEnleve, int augmentationDegats) {
        this.id = id;
        this.nom = nom;
        this.degats = degats;
        this.duree = duree;
        this.chanceBloquerAttaque = chanceBloquerAttaque;
        this.chanceEnleve = chanceEnleve;
        this.augmentationDegats = augmentationDegats;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public int getDegats() {
        return degats;
    }

    public int getDuree() {
        return duree;
    }

    public int getChanceBloquerAttaque() {
        return chanceBloquerAttaque;
    }

    public int getChanceEnleve() {
        return chanceEnleve;
    }

    public int getAugmentationDegats() {
        return augmentationDegats;
    }

    public void diminuerDuree(){
        this.duree--;
    }

    public void augmenterDegat(){
        this.degats+=this.augmentationDegats;
    }
}
