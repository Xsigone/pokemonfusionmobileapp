package com.example.pokemonfusion;

import java.util.ArrayList;
import java.util.List;

public class Utilisateur {
    private int id;
    private String pseudo;
    private String mdp;

    public Utilisateur(){
        this.id = 0;
        this.pseudo = "null";
        this.mdp = "null";
    }

    public Utilisateur(int id, String pseudo, String mdp){
        this.id = id;
        this.pseudo = pseudo;
        this.mdp = mdp;
    }

    public int getId(){return this.id;}
    public String getPseudo() {
        return pseudo;
    }

    public String getMdp() {
        return mdp;
    }

}
