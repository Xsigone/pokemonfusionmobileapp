package com.example.pokemonfusion;

import java.io.Serializable;

public class Stat implements Cloneable, Serializable {
    private int pv;
    private float attaque, defense, attaqueSpe, defenseSpe, vitesse;

    public Stat(){}

    public Stat(int pv, float attaque, float defense, float attaqueSpe, float defenseSpe, float vitesse) {
        this.pv = pv;
        this.attaque = attaque;
        this.defense = defense;
        this.attaqueSpe = attaqueSpe;
        this.defenseSpe = defenseSpe;
        this.vitesse = vitesse;
    }

    public int getPv() {
        return pv;
    }

    public float getAttaque() {
        return attaque;
    }

    public float getDefense() {
        return defense;
    }

    public float getAttaqueSpe() {
        return attaqueSpe;
    }

    public float getDefenseSpe() {
        return defenseSpe;
    }

    public float getVitesse() {
        return vitesse;
    }

    public void setPv(int pv) {
        this.pv = pv;
    }

    public void setAttaque(float attaque) {
        this.attaque = attaque;
    }

    public void setDefense(float defense) {
        this.defense = defense;
    }

    public void setAttaqueSpe(float attaqueSpe) {
        this.attaqueSpe = attaqueSpe;
    }

    public void setDefenseSpe(float defenseSpe) {
        this.defenseSpe = defenseSpe;
    }

    public void setVitesse(float vitesse) {
        this.vitesse = vitesse;
    }

    public void ajoute(Stat stat){
        this.pv *= stat.getPv();
        this.attaque *= stat.getAttaque();
        this.defense *= stat.getDefense();
        this.attaqueSpe *= stat.getAttaqueSpe();
        this.defenseSpe *= stat.getDefenseSpe();
        this.vitesse *= stat.getVitesse();
    }
    @Override
    public Object clone(){
        return new Stat(this.pv, this.attaque, this.defense, this.attaqueSpe, this.defenseSpe, this.vitesse);
    }
}
