package com.example.pokemonfusion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;

public class ModifTeam extends AppCompatActivity {
    //private MediaPlayer mediaPlayer;
    private DBOpenHelper dbOpenHelper;
    private int idUt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modifteam);
        // Ouverture de la base de données
        dbOpenHelper = new DBOpenHelper(this);
        try {
            dbOpenHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            dbOpenHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }
        Intent intent = getIntent();
        String idTeam = intent.getStringExtra("idTeam");
        int idteam = Integer.parseInt(idTeam.trim());

        this.idUt = intent.getIntExtra("idUt", 1);
        TextView nomEquipe = findViewById(R.id.nomEquipe);
        Equipe equipe = dbOpenHelper.getEquipeById(idteam);
        nomEquipe.setText(equipe.getNom());
        // pokemon 1
        TextView nomP1 = findViewById(R.id.textpoke1);
        ImageView img1 = findViewById(R.id.imgpkm1);
        Button bout1 = findViewById(R.id.button1);
        nomP1.setText(equipe.getListePokemons().get(0).getNom());
        String nomimgpokemon1=equipe.getListePokemons().get(0).getNom().toLowerCase();
        img1.setImageResource(getResources().getIdentifier(nomimgpokemon1, "drawable", getPackageName()));
        bout1.setContentDescription(equipe.getListePokemons().get(0).getId()+"");
        // pokemon 2
        TextView nomP2 = findViewById(R.id.textpoke2);
        ImageView img2 = findViewById(R.id.imgpkm2);
        Button bout2 = findViewById(R.id.atk2);
        nomP2.setText(equipe.getListePokemons().get(1).getNom());
        String nomimgpokemon2=equipe.getListePokemons().get(1).getNom().toLowerCase();
        img2.setImageResource(getResources().getIdentifier(nomimgpokemon2, "drawable", getPackageName()));
        bout2.setContentDescription(equipe.getListePokemons().get(1).getId()+"");

        // pokemon 3
        TextView nomP3 = findViewById(R.id.textpoke3);
        ImageView img3 = findViewById(R.id.imgpkm3);
        Button bout3 = findViewById(R.id.atk3);
        nomP3.setText(equipe.getListePokemons().get(2).getNom());
        String nomimgpokemon3=equipe.getListePokemons().get(2).getNom().toLowerCase();
        img3.setImageResource(getResources().getIdentifier(nomimgpokemon3, "drawable", getPackageName()));
        bout3.setContentDescription(equipe.getListePokemons().get(2).getId()+"");

        // pokemon 4
        TextView nomP4 = findViewById(R.id.textpoke4);
        ImageView img4 = findViewById(R.id.imgpkm4);
        Button bout4 = findViewById(R.id.atk4);
        nomP4.setText(equipe.getListePokemons().get(3).getNom());
        String nomimgpokemon4=equipe.getListePokemons().get(3).getNom().toLowerCase();
        img4.setImageResource(getResources().getIdentifier(nomimgpokemon4, "drawable", getPackageName()));
        bout4.setContentDescription(equipe.getListePokemons().get(3).getId()+"");

        // pokemon 5
        TextView nomP5 = findViewById(R.id.textpoke5);
        ImageView img5 = findViewById(R.id.imgpkm5);
        Button bout5 = findViewById(R.id.button5);
        nomP5.setText(equipe.getListePokemons().get(4).getNom());
        String nomimgpokemon5=equipe.getListePokemons().get(4).getNom().toLowerCase();
        img5.setImageResource(getResources().getIdentifier(nomimgpokemon5, "drawable", getPackageName()));
        bout5.setContentDescription(equipe.getListePokemons().get(4).getId()+"");

        // pokemon 6
        TextView nomP6 = findViewById(R.id.textpoke6);
        ImageView img6 = findViewById(R.id.imgpkm6);
        Button bout6 = findViewById(R.id.button6);
        nomP6.setText(equipe.getListePokemons().get(5).getNom());
        String nomimgpokemon6=equipe.getListePokemons().get(5).getNom().toLowerCase();
        img6.setImageResource(getResources().getIdentifier(nomimgpokemon6, "drawable", getPackageName()));
        bout6.setContentDescription(equipe.getListePokemons().get(5).getId()+"");


    }

    public void retour(View view){
        Intent intentrecup = getIntent();
        String teamSelect = intentrecup.getStringExtra("teamselecte");
        Intent intent = new Intent(this, SelectTeam.class);
        intent.putExtra("idUt", this.idUt);
        intent.putExtra("teamselecte",teamSelect);
        startActivity(intent);
    }

    public void selectionner(View view){
        Intent intentrec = getIntent();
        String idTeam = intentrec.getStringExtra("idTeam");
        int idteam = Integer.parseInt(idTeam.trim());
        Intent intent = new Intent(this, Acc.class);
        intent.putExtra("teamselecte",idteam+"");
        startActivity(intent);
    }
    public void modifier(View view){
        Intent intentrecup = getIntent();
        String teamSelect = intentrecup.getStringExtra("teamselecte");

        String idpokemonstring = (String)view.getContentDescription();
        int idpoke = Integer.parseInt(idpokemonstring.trim());

        Intent intentrec = getIntent();
        String idTeam = intentrec.getStringExtra("idTeam");

        Intent intent = new Intent(this, ModifPokemon.class);
        intent.putExtra("idTeam", idTeam);
        intent.putExtra("idPoke", idpoke);
        intent.putExtra("teamselecte",teamSelect);

        startActivity(intent);
    }

}
