package com.example.pokemonfusion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Hashtable;

public class CreerCompte extends AppCompatActivity {
    //private MediaPlayer mediaPlayer;
    private DBOpenHelper dbOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.creercompte);
        // Ouverture de la base de données
        dbOpenHelper = new DBOpenHelper(this);
        try {
            dbOpenHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            dbOpenHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

    }

    public void valider(View view){
        EditText ndc = findViewById(R.id.champNDC);
        EditText mdp = findViewById(R.id.champMDP);
        EditText mdv = findViewById(R.id.champMDPV);
        if(ndc.getText().toString().length()>2) {
            if (mdp.getText().toString().equals(mdv.getText().toString()) && mdp.getText().toString().length()>0) {
                dbOpenHelper.ajouterUtilisateur(ndc.getText().toString(), mdp.getText().toString());
                Intent intent = new Intent(this, Pageconnexion.class);
                startActivity(intent);
            } else {
                Toast toast = Toast.makeText(getApplicationContext(), "Les 2 mdps ne correspondent pas ou le mdp est trop court", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(), "Le nom d'utlisateur est trop court. Il faut au moins 3 charactères", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
    public void annuler(View view){
        Intent intent = new Intent(this, Pageconnexion.class);
        startActivity(intent);
    }

}
