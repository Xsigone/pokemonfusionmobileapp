package com.example.pokemonfusion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class SwitchPokemon extends AppCompatActivity {
    //private MediaPlayer mediaPlayer;
    private DBOpenHelper dbOpenHelper;
    private GestionCombat combat;
    private String swi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.switchpkm);

        dbOpenHelper = new DBOpenHelper(this);
        try {
            dbOpenHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            dbOpenHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        Intent intent = getIntent();

        this.combat = (GestionCombat)intent.getSerializableExtra("combat");
        this.swi = intent.getStringExtra("switch");
        Equipe equipe = combat.getEquipe1();

        ImageView imgpkm1 = findViewById(R.id.imgpkm1);
        ImageView imgpkm2 = findViewById(R.id.imgpkm2);
        ImageView imgpkm3 = findViewById(R.id.imgpkm3);
        ImageView imgpkm4 = findViewById(R.id.imgpkm4);
        ImageView imgpkm5 = findViewById(R.id.imgpkm5);
        ImageView imgpkm6 = findViewById(R.id.imgpkm6);

        imgpkm1.setImageResource(getResources().getIdentifier(equipe.getListePokemons().get(0).getNom().toLowerCase(), "drawable", getPackageName()));
        imgpkm2.setImageResource(getResources().getIdentifier(equipe.getListePokemons().get(1).getNom().toLowerCase(), "drawable", getPackageName()));
        imgpkm3.setImageResource(getResources().getIdentifier(equipe.getListePokemons().get(2).getNom().toLowerCase(), "drawable", getPackageName()));
        imgpkm4.setImageResource(getResources().getIdentifier(equipe.getListePokemons().get(3).getNom().toLowerCase(), "drawable", getPackageName()));
        imgpkm5.setImageResource(getResources().getIdentifier(equipe.getListePokemons().get(4).getNom().toLowerCase(), "drawable", getPackageName()));
        imgpkm6.setImageResource(getResources().getIdentifier(equipe.getListePokemons().get(5).getNom().toLowerCase(), "drawable", getPackageName()));


        TextView pkm1 = findViewById(R.id.pkm1);
        TextView pkm2 = findViewById(R.id.pkm2);
        TextView pkm3 = findViewById(R.id.pkm3);
        TextView pkm4 = findViewById(R.id.pkm4);
        TextView pkm5 = findViewById(R.id.pkm5);
        TextView pkm6 = findViewById(R.id.pkm6);

        pkm1.setText(equipe.getListePokemons().get(0).getNom());
        pkm2.setText(equipe.getListePokemons().get(1).getNom());
        pkm3.setText(equipe.getListePokemons().get(2).getNom());
        pkm4.setText(equipe.getListePokemons().get(3).getNom());
        pkm5.setText(equipe.getListePokemons().get(4).getNom());
        pkm6.setText(equipe.getListePokemons().get(5).getNom());

        Button b1 = findViewById(R.id.button1);
        Button b2 = findViewById(R.id.button2);
        Button b3 = findViewById(R.id.button3);
        Button b4 = findViewById(R.id.button4);
        Button b5 = findViewById(R.id.button5);
        Button b6 = findViewById(R.id.button6);

        b1.setContentDescription(equipe.getListePokemons().get(0).getId()+"");
        b2.setContentDescription(equipe.getListePokemons().get(1).getId()+"");
        b3.setContentDescription(equipe.getListePokemons().get(2).getId()+"");
        b4.setContentDescription(equipe.getListePokemons().get(3).getId()+"");
        b5.setContentDescription(equipe.getListePokemons().get(4).getId()+"");
        b6.setContentDescription(equipe.getListePokemons().get(5).getId()+"");

        ProgressBar p1 = findViewById(R.id.progressBar1);
        ProgressBar p2 = findViewById(R.id.progressBar3);
        ProgressBar p3 = findViewById(R.id.progressBar4);
        ProgressBar p4 = findViewById(R.id.progressBar5);
        ProgressBar p5 = findViewById(R.id.progressBar6);
        ProgressBar p6 = findViewById(R.id.progressBar7);

        p1.setProgress((int)(((float)equipe.getListePokemons().get(0).getPvActuel() / (float)equipe.getListePokemons().get(0).getStat().getPv()) * 100), true);
        p2.setProgress((int)(((float)equipe.getListePokemons().get(1).getPvActuel() / (float)equipe.getListePokemons().get(1).getStat().getPv()) * 100), true);
        p3.setProgress((int)(((float)equipe.getListePokemons().get(2).getPvActuel() / (float)equipe.getListePokemons().get(2).getStat().getPv()) * 100), true);
        p4.setProgress((int)(((float)equipe.getListePokemons().get(3).getPvActuel() / (float)equipe.getListePokemons().get(3).getStat().getPv()) * 100), true);
        p5.setProgress((int)(((float)equipe.getListePokemons().get(4).getPvActuel() / (float)equipe.getListePokemons().get(4).getStat().getPv()) * 100), true);
        p6.setProgress((int)(((float)equipe.getListePokemons().get(5).getPvActuel() / (float)equipe.getListePokemons().get(5).getStat().getPv()) * 100), true);

        //Lancement de la musique au lancement du jeux
        //this.mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.monsons);
        //mediaPlayer.start();

    }

    //fonction retour présente sur la page switchpkm permettant de revenir a main
    public void backmain(View view){
        if (this.swi.equals("ko")) {
            Toast toast = Toast.makeText(getApplicationContext(), "Vous devez choisir un nouveau pokemon !", Toast.LENGTH_SHORT);
            toast.show();
        }else{
            Intent intent = new Intent(this, Combat.class);
            intent.putExtra("combat", this.combat);
            startActivity(intent);
        }
    }

    //fonction permettant de changer de pokemon
    public void changerPokemon(View view){
        String idpokes = (String)view.getContentDescription();
        int idpoke = Integer.parseInt(idpokes.trim());
        for (Pokemon poke : this.combat.getEquipe1().getListePokemons()) {
            int idpokeActif = this.combat.getPokemonEquipe1().getId();
            int idpokeBoucle = poke.getId();
            if(idpokeBoucle==idpoke) {
                if (poke.isMort()) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Impossible, ce pokemon est KO !", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    if (idpoke == idpokeActif) {
                        Toast toast = Toast.makeText(getApplicationContext(), "Impossible, ce pokemon est déja sur le terrain !", Toast.LENGTH_SHORT);
                        toast.show();
                    } else {
                        this.combat.setPokemonEquipe1(poke);
                        if(!this.swi.equals("ko")) {
                            String efficace2 = this.combat.attaqueAdverse(this.combat.getPokemonEquipe2().getAttaque1());
                            if (!efficace2.equals("")){
                                efficace2 = " et " + efficace2;
                            }
                            String texte2 = "Le "+this.combat.getPokemonEquipe2().getNom() + " adverse utilise "+this.combat.getPokemonEquipe2().getAttaque1().getNom() + efficace2;
                            Toast toast2 = Toast.makeText(getApplicationContext(), texte2,Toast.LENGTH_SHORT);
                            toast2.show();
                        }
                        Intent intent = new Intent(this, Combat.class);
                        intent.putExtra("combat", this.combat);
                        startActivity(intent);
                    }
                }
            }
        }

    }

}
