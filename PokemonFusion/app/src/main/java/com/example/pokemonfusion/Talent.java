package com.example.pokemonfusion;

import java.io.Serializable;

class Talent implements Serializable {

    private int id;
    private String nom;
    private int immuElec, immuEau, regen, condVie, survie1Hp, nouveauxStab, augmenterVitesse;

    public Talent() {
    }

    public Talent(int id, String nom, int immuElec, int immuEau, int regen, int condVie, int survie1Hp, int nouveauxStab, int augmenterVitesse) {
        this.id = id;
        this.nom = nom;
        this.immuElec = immuElec;
        this.immuEau = immuEau;
        this.regen = regen;
        this.condVie = condVie;
        this.survie1Hp = survie1Hp;
        this.nouveauxStab = nouveauxStab;
        this.augmenterVitesse = augmenterVitesse;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public int getImmuElec() {
        return immuElec;
    }

    public int getImmuEau() {
        return immuEau;
    }

    public int getRegen() {
        return regen;
    }

    public int getCondVie() {
        return condVie;
    }

    public int getSurvie1Hp() {
        return survie1Hp;
    }

    public int getNouveauxStab() {
        return nouveauxStab;
    }

    public int getAugmenterVitesse() {
        return augmenterVitesse;
    }
}
