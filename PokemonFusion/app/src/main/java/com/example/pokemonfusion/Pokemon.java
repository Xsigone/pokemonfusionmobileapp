package com.example.pokemonfusion;

import java.io.Serializable;

public class Pokemon implements Serializable {
    private int id;
    private String nom;
    private Type type1;
    private Type type2;
    private int pvActuel;
    private Stat stat;
    private Stat boost;
    private Objet objet;
    private Statut statut;
    private Talent talent;
    private Attaque attaque1, attaque2, attaque3, attaque4;
    private boolean mort;

    public Pokemon() {
    }

    public Pokemon(int id, String nom, Type type1, Type type2, Stat stat) {
        this.id = id;
        this.nom = nom;
        this.type1 = type1;
        this.type2 = type2;
        this.pvActuel = stat.getPv();
        this.stat = stat;
        this.boost = (Stat)stat.clone();
        this.mort = false;
    }

    public Pokemon(int id, String nom, Type type1, Type type2, Stat stat, Objet objet, Talent talent, Attaque attaque1, Attaque attaque2, Attaque attaque3, Attaque attaque4, Statut statut){
        this.id = id;
        this.nom = nom;
        this.type1 = type1;
        this.type2 = type2;
        this.pvActuel = stat.getPv();
        this.stat = stat;
        this.objet = objet;
        this.talent = talent;
        this.attaque1 = attaque1;
        this.attaque2 = attaque2;
        this.attaque3 = attaque3;
        this.attaque4 = attaque4;
        this.statut = statut;
        this.boost = new Stat(1, 1, 1, 1, 1, 1);
        this.mort = false;
    }

    public int getId(){return id;}
    public String getNom() {
        return nom;
    }

    public Type getType1() {
        return type1;
    }

    public Type getType2() {
        return type2;
    }

    public int getPvActuel() { return pvActuel; }

    public Objet getObjet() {
        return objet;
    }

    public Talent getTalent() {
        return talent;
    }

    public Attaque getAttaque1() {
        return attaque1;
    }

    public Attaque getAttaque2() {
        return attaque2;
    }

    public Attaque getAttaque3() {
        return attaque3;
    }

    public Attaque getAttaque4() {
        return attaque4;
    }

    public Stat getStat() { return stat; }

    public Stat getBoost() { return boost; }

    public Statut getStatut() { return statut; }

    public boolean isMort() {return mort;}

    public void setPvActuel(int pvActuel) { this.pvActuel = pvActuel; }

    public void setObjet(Objet objet) {
        this.objet = objet;
    }

    public void setStatus(Statut statut) {
        this.statut = statut;
    }

    public void setMort(boolean mort) { this.mort = mort; }

    public void subirDegats(int degats){
        this.pvActuel -= degats;
        if(this.pvActuel<0){
            this.pvActuel=0;
        }
    }

    public void soigne(int vie){
        this.pvActuel += vie;
        if(this.pvActuel>this.getStat().getPv()){
            this.pvActuel=this.getStat().getPv();
        }
    }
}

