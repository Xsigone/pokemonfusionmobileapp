# PokemonFusionMobileApp

Voici notre application pokemon Fusion !

Dans celle-ci, vous pourrez effectuer des combats contre une I.A dans des combats de pokemons.

Lorsque l'utilisateur lancera l'application, il aura une petite animation avec un Carapuce. En cliquant sur l'image, il aurez acces au menu de connexion.
Il pourra aussi y crée un compte en y renseignant un nom de compte comprenant plus de 3 caractères, et deux mots de passe identiques.
Une fois que vous vous serez connectés, vous arriverez sur l'accueil.
Ici, l'utilisateur aura 3 choix : - se déconnecter et donc revenir au menu de connexion.
				  - selectionner une team.
                                  - combattre.

lorsque l'utilisateur sera dans le menue de sélection de team, il y aura un bouton permettant de genrer la team aléatoirement.
Il pourra alors en voir le contenue (voir tous les pokemons de la team), et voir le détail de chaque pokemon (les attaques, les objets et le talent).

Si les pokemons dedans ne lui convient, il pourra alors sélectionner la team et accéder au menu de combat. En effet, l'utilisateur doit choisir une équipe
pour pouvoir combattre.

Si la team ne convient pas a l'utilisateur, il pourra faire retour, et revoir les détails de sa team. Elle aura donc changée.

Une fois en combat, le but est simple : battre tous les pokemons de l'adversaire.
Pour ça votre pokemon dispose de plusieurs attaques, certaines augmentant les statitisques du pokemon, et d'autres faisant des dommages à l'adversaire.

Il faut maintenant faire place à la stratégie afin de vaincre l'adversaire ! 